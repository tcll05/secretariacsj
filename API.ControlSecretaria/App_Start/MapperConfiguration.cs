﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using API.ControlSecretaria.EFModel;
using API.ControlSecretaria.Models;
using AutoMapper;

namespace API.ControlSecretaria.App_Start
{
    public class MapperConfig
    {
        public MapperConfig()
        {
            
        }
        public static IMapper config()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<salas, SalasViewModel>());
            var mapper = config.CreateMapper();
            return mapper;
        }
    }
}