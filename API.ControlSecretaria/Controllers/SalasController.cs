﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API.ControlSecretaria.App_Start;
using API.ControlSecretaria.EFModel;
using API.ControlSecretaria.Models;
using AutoMapper;

namespace API.ControlSecretaria.Controllers
{
    [Route("api/[controller]")]
    public class SalasController : ApiController
    {
        IMapper mapper;
        private secretaria_devEntities db = new secretaria_devEntities();


        // GET: api/Salas
        [HttpGet]
        public IEnumerable<SalasViewModel> Getsalas()
        {
            var res = from c in db.salas.AsNoTracking()
                      select new SalasViewModel
                      {
                          descripcion = c.descripcion,
                          estado = c.estado,
                          usuario_registro = c.usuario_registro
                      };
            return res.ToList();
        }

        // GET: api/Salas/5
        [ResponseType(typeof(SalasViewModel))]
        //[HttpGet("{id}")]
        public IHttpActionResult Get(int id)
        {
            mapper = MapperConfig.config();
            salas salas = db.salas.Find(id);
            if (salas == null)
            {
                return NotFound();
            }
            var sala = mapper.Map<SalasViewModel>(salas);
            return Ok(sala);
        }

        // PUT: api/Salas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putsalas(int id, salas salas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != salas.id_salas)
            {
                return BadRequest();
            }

            db.Entry(salas).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!salasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Salas
        [ResponseType(typeof(salas))]
        public IHttpActionResult Postsalas(salas salas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.salas.Add(salas);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = salas.id_salas }, salas);
        }

        // DELETE: api/Salas/5
        [ResponseType(typeof(salas))]
        public IHttpActionResult Deletesalas(int id)
        {
            salas salas = db.salas.Find(id);
            if (salas == null)
            {
                return NotFound();
            }

            db.salas.Remove(salas);
            db.SaveChanges();

            return Ok(salas);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool salasExists(int id)
        {
            return db.salas.Count(e => e.id_salas == id) > 0;
        }
    }
}