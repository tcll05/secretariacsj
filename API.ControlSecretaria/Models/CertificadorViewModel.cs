﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.ControlSecretaria.Models
{
    public class CertificadorViewModel
    {
        public string nombre_certificador { set; get; }
        public string puesto { set; get; }
        public bool estado { set; get; }
        public string usuario_registro { set; get; }
    }
}