﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.ControlSecretaria.Models
{
    public class ObservacionCasacionViewModel
    {
        public string descripcion { set; get; }
        public bool estado { set; get; }
        public string usuario_registro { set; get; }
        public int id_casacion { set; get; }
        public int id_tipo_observacion { set; get; }
        public CasacionViewModel casacion { set; get; }
        public TipoObservacionViewModel tipoObservacion { set; get; }
    }
}