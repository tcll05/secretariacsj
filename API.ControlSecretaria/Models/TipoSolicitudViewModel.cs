﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.ControlSecretaria.Models
{
    public class TipoSolicitudViewModel
    {
        public string descripcion { set; get; }
        public bool estado { set; get; }
        public string usuario_registro { set; get; }
    }
}