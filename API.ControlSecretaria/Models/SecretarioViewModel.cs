﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.ControlSecretaria.Models
{
    public class SecretarioViewModel
    {
        public string nombre { set; get; }
        public string puesto { set; get; }
        public string usuario_registro { set; get; }

    }
}