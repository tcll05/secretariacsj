﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.ControlSecretaria.Models
{
    public class CasacionViewModel
    {
        public string numero_registro { set; get; }
        public string nombre_apoderado { set; get; }
        public string nombre_demandante { set; get; }
        public string nombre_demandado { set; get; }
        public string motivo_demanda { set; get; }
        public string resolucion { set; get; }
        public bool completo { set; get; }
        public int numero_correlativo { set; get; }
        public string consta_de { set; get; }
        public string descripcion { set; get; }
        public bool estado { set; get; }
        public string usuario_registro { set; get; }
        public int id_tipo_casacion { set; get; }
        public int id_juzgado { set; get; }
        public int id_letrado { set; get; }
        public TipoCasacionViewModel tipoCasacion { set; get; }
        public JuzgadoViewModel juzgado { set;get; }
        public LetradoViewModel letrado { set; get; }

    }
}