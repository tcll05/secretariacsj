﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.ControlSecretaria.Models
{
    public class TipoCasacionViewModel
    {
        public string descripcion { set; get; }
        public bool estado { set; get; }
        public string usuario_registro { set; get; }
        public int id_sala { set; get; }
        public SalasViewModel salas { set; get; }
    }
}