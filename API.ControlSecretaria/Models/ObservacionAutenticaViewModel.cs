﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.ControlSecretaria.Models
{
    public class ObservacionAutenticaViewModel
    {
        public string descripcion { set; get; }
        public bool estado { set; get; }
        public string usuario_registro { set; get; }
        public int id_autentica_recepcion { set; get; }
        public int id_tipo_observacion { set; get; }
        public AutenticaRecepcionViewModel autenticaRecepcion { set; get; }
        public TipoObservacionViewModel tipoObservacion{set;get;}

    }
}