﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.ControlSecretaria.Models
{
    public class TipoObservacionViewModel
    {
        public string descripcion { set; get; }
        public bool estado { set; get; }
        public string usuario_registro { set; get; }
        public int id_tipo_transaccion { set; get; }
        public TipoTransaccionViewModel tipoTransaccion { set; get; }
    }
}