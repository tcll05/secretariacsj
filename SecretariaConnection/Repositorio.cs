﻿using SecretariaConnection.EFModel;
using SecretariaConnection.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecretariaConnection
{
    public class Repositorio
    {
        #region salas
        public List<SalasViewModel> getSalas()
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<SalasViewModel> a = (from r in c.salas
                                              select new SalasViewModel
                                              {
                                                  id_salas = r.id_salas,
                                                  descripcion = r.descripcion,
                                                  estado = r.estado,
                                                  usuario_registro = r.usuario_registro,
                                                  alias = r.alias
                                              }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public SalasViewModel buscarSala(int id)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    SalasViewModel a = (from r in c.salas.AsNoTracking()
                                        where r.id_salas.Equals(id)
                                        select new SalasViewModel
                                        {
                                            descripcion = r.descripcion,
                                            alias = r.alias,
                                            id_salas = r.id_salas,
                                            estado = r.estado,
                                            usuario_registro = r.usuario_registro
                                        }).FirstOrDefault();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public string setSalas(salas sala, EntityState estado) //agregar o modificar
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(sala).State = estado;
                        c.SaveChanges();
                        return "Se ingreso correctamente el registro";
                    }
                    else
                    {
                        c.Entry(sala).State = estado;
                        c.SaveChanges();
                        return "Se modifico correctamente el registro";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Juzgado
        public List<JuzgadoViewModel> getJuzgado()
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<JuzgadoViewModel> a = (from r in c.juzgado
                                                select new JuzgadoViewModel
                                                {
                                                    id_juzgado = r.id_juzgado,
                                                    descripcion = r.descripcion,
                                                    estado = r.estado,
                                                    usuario_registro = r.usuario_registro
                                                }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public JuzgadoViewModel buscarJuzgado(int id)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    JuzgadoViewModel a = (from r in c.juzgado.AsNoTracking()
                                          where r.id_juzgado.Equals(id)
                                          select new JuzgadoViewModel
                                          {
                                              descripcion = r.descripcion,
                                              estado = r.estado,
                                              usuario_registro = r.usuario_registro
                                          }).FirstOrDefault();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public string setJuzgado(juzgado proc, EntityState estado) //agregar o modificar
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(proc).State = estado;
                        c.SaveChanges();
                        return "Se ingreso correctamente el registro";
                    }
                    else
                    {
                        c.Entry(proc).State = estado;
                        c.SaveChanges();
                        return "Se modifico correctamente el registro";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region TipoSolicitud
        public List<TipoSolicitudViewModel> getTipoSolicitud()
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<TipoSolicitudViewModel> a = (from r in c.tipo_solicitud
                                                      select new TipoSolicitudViewModel
                                                      {
                                                          id_tipo_solicitud = r.id_tipo_solicitud,
                                                          descripcion = r.descripcion,
                                                          estado = r.estado,
                                                          usuario_registro = r.usuario_registro
                                                      }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public TipoSolicitudViewModel buscarTipoSolicitud(int id)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    TipoSolicitudViewModel a = (from r in c.tipo_solicitud.AsNoTracking()
                                                where r.id_tipo_solicitud.Equals(id)
                                                select new TipoSolicitudViewModel
                                                {
                                                    descripcion = r.descripcion,
                                                    estado = r.estado,
                                                    usuario_registro = r.usuario_registro
                                                }).FirstOrDefault();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public string setTipoSolicitud(tipo_solicitud proc, EntityState estado) //agregar o modificar
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(proc).State = estado;
                        c.SaveChanges();
                        return "Se ingreso correctamente el registro";
                    }
                    else
                    {
                        c.Entry(proc).State = estado;
                        c.SaveChanges();
                        return "Se modifico correctamente el registro";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region TipoCasaciones
        public List<TipoCasacionViewModel> getTipoCasacion(int id)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<TipoCasacionViewModel> a = (from r in c.tipo_casacion
                                                     join y in c.salas on r.id_sala equals y.id_salas
                                                     where r.id_sala.Equals(id)
                                                     select new TipoCasacionViewModel
                                                     {
                                                         id_tipo_casacion = r.id_tipo_casacion,
                                                         id_salas = r.salas.id_salas,
                                                         descripcion = r.descripcion,
                                                         estado = r.estado,
                                                         usuario_registro = r.usuario_registro,
                                                         salas = new SalasViewModel
                                                         {
                                                             id_salas = y.id_salas,
                                                             descripcion = y.descripcion,
                                                             estado = y.estado,
                                                             usuario_registro = y.usuario_registro
                                                         }
                                                     }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public List<TipoCasacionViewModel> getTipoCasacion()
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<TipoCasacionViewModel> a = (from r in c.tipo_casacion
                                                     join y in c.salas on r.id_sala equals y.id_salas
                                                     select new TipoCasacionViewModel
                                                     {
                                                         id_tipo_casacion = r.id_tipo_casacion,
                                                         id_salas = r.salas.id_salas,
                                                         descripcion = r.descripcion,
                                                         estado = r.estado,
                                                         usuario_registro = r.usuario_registro,
                                                         salas = new SalasViewModel
                                                         {
                                                             id_salas = y.id_salas,
                                                             descripcion = y.descripcion,
                                                             estado = y.estado,
                                                             usuario_registro = y.usuario_registro
                                                         }
                                                     }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public TipoCasacionViewModel buscarTipoCasacion(int id)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    TipoCasacionViewModel a = (from r in c.tipo_casacion.AsNoTracking()
                                               join y in c.salas on r.id_sala equals y.id_salas
                                               where r.id_tipo_casacion.Equals(id)
                                               select new TipoCasacionViewModel
                                               {
                                                   id_tipo_casacion = r.id_tipo_casacion,
                                                   id_salas = r.salas.id_salas,
                                                   descripcion = r.descripcion,
                                                   estado = r.estado,
                                                   usuario_registro = r.usuario_registro,
                                                   salas = new SalasViewModel
                                                   {
                                                       id_salas = y.id_salas,
                                                       descripcion = y.descripcion,
                                                       estado = y.estado,
                                                       usuario_registro = y.usuario_registro
                                                   }
                                               }).FirstOrDefault();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public string setTipoCasacion(tipo_casacion casacion, EntityState estado) //agregar o modificar
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(casacion).State = estado;
                        c.SaveChanges();
                        return "Se ingreso correctamente el registro";
                    }
                    else
                    {
                        c.Entry(casacion).State = estado;
                        c.SaveChanges();
                        return "Se modifico correctamente el registro";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Letrados
        public List<LetradoViewModel> getLetrado()
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<LetradoViewModel> a = (from r in c.letrado
                                                select new LetradoViewModel
                                                {
                                                    id_letrado = r.id_letrado,
                                                    descripcion = r.descripcion,
                                                    numero_letrado = r.numero_letrado,
                                                    estado = r.estado,
                                                    usuario_registro = r.usuario_registro
                                                }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public LetradoViewModel buscarLetrado(int id)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    LetradoViewModel a = (from r in c.letrado.AsNoTracking()
                                          where r.id_letrado.Equals(id)
                                          select new LetradoViewModel
                                          {
                                              descripcion = r.descripcion,
                                              numero_letrado = r.numero_letrado,
                                              id_letrado = r.id_letrado,
                                              estado = r.estado,
                                              usuario_registro = r.usuario_registro
                                          }).FirstOrDefault();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public string setLetrado(letrado sala, EntityState estado) //agregar o modificar
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(sala).State = estado;
                        c.SaveChanges();
                        return "Se ingreso correctamente el registro";
                    }
                    else
                    {
                        c.Entry(sala).State = estado;
                        c.SaveChanges();
                        return "Se modifico correctamente el registro";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region TipoFechas

        public List<TipoFechaViewModel> getTipoFecha(int transaccion)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<TipoFechaViewModel> a = (from r in c.tipo_fecha
                                                  join y in c.tipo_transaccion on r.id_tipo_transaccion equals y.id_tipo_transaccion
                                                  where r.id_tipo_transaccion.Equals(transaccion)
                                                  select new TipoFechaViewModel
                                                  {
                                                      id_tipo_fecha = r.id_tipo_fecha,
                                                      id_tipo_transaccion = r.tipo_transaccion.id_tipo_transaccion,
                                                      descripcion = r.descripcion,
                                                      estado = r.estado,
                                                      usuario_registro = r.usuario_registro,
                                                      tipoTransaccion = new TipoTransaccionViewModel
                                                      {
                                                          id_tipo_transaccion = y.id_tipo_transaccion,
                                                          descripcion = y.descripcion,
                                                          estado = y.estado,
                                                          usuario_registro = y.usuario_registro
                                                      }
                                                  }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public List<TipoFechaViewModel> getTipoFecha()
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<TipoFechaViewModel> a = (from r in c.tipo_fecha
                                                  join y in c.tipo_transaccion on r.id_tipo_transaccion equals y.id_tipo_transaccion
                                                  //where r.id_tipo_transaccion.Equals(transaccion)
                                                  select new TipoFechaViewModel
                                                  {
                                                      id_tipo_fecha = r.id_tipo_fecha,
                                                      id_tipo_transaccion = r.tipo_transaccion.id_tipo_transaccion,
                                                      descripcion = r.descripcion,
                                                      estado = r.estado,
                                                      usuario_registro = r.usuario_registro,
                                                      tipoTransaccion = new TipoTransaccionViewModel
                                                      {
                                                          id_tipo_transaccion = y.id_tipo_transaccion,
                                                          descripcion = y.descripcion,
                                                          estado = y.estado,
                                                          usuario_registro = y.usuario_registro
                                                      }
                                                  }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public TipoFechaViewModel buscarTipoFecha(int id)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    TipoFechaViewModel a = (from r in c.tipo_fecha
                                            join y in c.tipo_transaccion on r.id_tipo_transaccion equals y.id_tipo_transaccion
                                            where r.id_tipo_fecha.Equals(id)
                                            select new TipoFechaViewModel
                                            {
                                                id_tipo_fecha = r.id_tipo_fecha,
                                                id_tipo_transaccion = r.tipo_transaccion.id_tipo_transaccion,
                                                descripcion = r.descripcion,
                                                estado = r.estado,
                                                usuario_registro = r.usuario_registro,
                                                tipoTransaccion = new TipoTransaccionViewModel
                                                {
                                                    id_tipo_transaccion = y.id_tipo_transaccion,
                                                    descripcion = y.descripcion,
                                                    estado = y.estado,
                                                    usuario_registro = y.usuario_registro
                                                }
                                            }).FirstOrDefault();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public string setTipoFecha(tipo_fecha fecha, EntityState estado) //agregar o modificar
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(fecha).State = estado;
                        c.SaveChanges();
                        return "Se ingreso correctamente el registro";
                    }
                    else
                    {
                        c.Entry(fecha).State = estado;
                        c.SaveChanges();
                        return "Se modifico correctamente el registro";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region TipoObservaciones
        public List<TipoObservacionViewModel> getTipoObservacion(int transaccion)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<TipoObservacionViewModel> a = (from r in c.tipo_observacion
                                                        join y in c.tipo_transaccion on r.id_tipo_transaccion equals y.id_tipo_transaccion
                                                        where r.id_tipo_transaccion.Equals(transaccion)
                                                        select new TipoObservacionViewModel
                                                        {
                                                            id_tipo_observacion = r.id_tipo_observacion,
                                                            id_tipo_transaccion = r.tipo_transaccion.id_tipo_transaccion,
                                                            descripcion = r.descripcion,
                                                            estado = r.estado,
                                                            usuario_registro = r.usuario_registro,
                                                            tipoTransaccion = new TipoTransaccionViewModel
                                                            {
                                                                id_tipo_transaccion = y.id_tipo_transaccion,
                                                                descripcion = y.descripcion,
                                                                estado = y.estado,
                                                                usuario_registro = y.usuario_registro
                                                            }
                                                        }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public List<TipoObservacionViewModel> getTipoObservacion()
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<TipoObservacionViewModel> a = (from r in c.tipo_observacion
                                                        join y in c.tipo_transaccion on r.id_tipo_transaccion equals y.id_tipo_transaccion
                                                        //where r.id_tipo_transaccion.Equals(transaccion)
                                                        select new TipoObservacionViewModel
                                                        {
                                                            id_tipo_observacion = r.id_tipo_observacion,
                                                            id_tipo_transaccion = r.tipo_transaccion.id_tipo_transaccion,
                                                            descripcion = r.descripcion,
                                                            estado = r.estado,
                                                            usuario_registro = r.usuario_registro,
                                                            tipoTransaccion = new TipoTransaccionViewModel
                                                            {
                                                                id_tipo_transaccion = y.id_tipo_transaccion,
                                                                descripcion = y.descripcion,
                                                                estado = y.estado,
                                                                usuario_registro = y.usuario_registro
                                                            }
                                                        }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public TipoObservacionViewModel buscarTipoObservacion(int id)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    TipoObservacionViewModel a = (from r in c.tipo_observacion
                                                  join y in c.tipo_transaccion on r.id_tipo_transaccion equals y.id_tipo_transaccion
                                                  where r.id_tipo_observacion.Equals(id)
                                                  select new TipoObservacionViewModel
                                                  {
                                                      id_tipo_observacion = r.id_tipo_observacion,
                                                      id_tipo_transaccion = r.tipo_transaccion.id_tipo_transaccion,
                                                      descripcion = r.descripcion,
                                                      estado = r.estado,
                                                      usuario_registro = r.usuario_registro,
                                                      tipoTransaccion = new TipoTransaccionViewModel
                                                      {
                                                          id_tipo_transaccion = y.id_tipo_transaccion,
                                                          descripcion = y.descripcion,
                                                          estado = y.estado,
                                                          usuario_registro = y.usuario_registro
                                                      }
                                                  }).FirstOrDefault();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public string setTipoObservacion(tipo_observacion fecha, EntityState estado) //agregar o modificar
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(fecha).State = estado;
                        c.SaveChanges();
                        return "Se ingreso correctamente el registro";
                    }
                    else
                    {
                        c.Entry(fecha).State = estado;
                        c.SaveChanges();
                        return "Se modifico correctamente el registro";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region transacciones
        public List<TipoTransaccionViewModel> getTransacciones()
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<TipoTransaccionViewModel> a = (from r in c.tipo_transaccion
                                                        select new TipoTransaccionViewModel
                                                        {
                                                            id_tipo_transaccion = r.id_tipo_transaccion,
                                                            descripcion = r.descripcion,
                                                            estado = r.estado,
                                                            usuario_registro = r.usuario_registro
                                                        }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        #endregion

        #region Casacions

        public int correlativoCasaciones(int id_sala, string anio)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    int a = (from r in c.casacion
                             join y in c.tipo_casacion on r.id_tipo_casacion equals y.id_tipo_casacion
                             join x in c.juzgado on r.id_juzgado equals x.id_juzgado
                             join l in c.letrado on r.id_letrado equals l.id_letrado
                             where r.tipo_casacion.id_sala.Equals(id_sala) && r.fecha_registro.Year.ToString().Equals(anio)
                             select new CasacionViewModel
                             {
                                 id_casacion = r.id_casacion,

                             }).Count();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            //return 0;
        }

        public List<CasacionViewModel> getCasacion()
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<CasacionViewModel> a = (from r in c.casacion
                                                 join y in c.tipo_casacion on r.id_tipo_casacion equals y.id_tipo_casacion
                                                 join x in c.juzgado on r.id_juzgado equals x.id_juzgado
                                                 join l in c.letrado on r.id_letrado equals l.id_letrado
                                                 orderby y.id_sala, r.numero_correlativo
                                                 select new CasacionViewModel
                                                 {
                                                     id_casacion = r.id_casacion,
                                                     id_juzgado = r.juzgado.id_juzgado,
                                                     id_letrado = r.letrado.id_letrado,
                                                     id_tipo_casacion = r.tipo_casacion.id_tipo_casacion,
                                                     completo = r.completo,
                                                     consta_de = r.consta_de,
                                                     motivo_demanda = r.motivo_demanda,
                                                     nombre_apoderado = r.nombre_apoderado,
                                                     nombre_demandado = r.nombre_demandado,
                                                     nombre_demandante = r.nombre_demandante,
                                                     numero_correlativo = r.numero_correlativo,
                                                     numero_registro = r.numero_registro,
                                                     procedencia = r.procedencia,
                                                     resolucion = r.resolucion,
                                                     tipoCasacion = (from m in c.tipo_casacion
                                                                     join n in c.salas on m.id_sala equals n.id_salas
                                                                     where m.id_tipo_casacion.Equals(r.id_tipo_casacion)
                                                                     select new TipoCasacionViewModel
                                                                     {
                                                                         id_tipo_casacion = m.id_tipo_casacion,
                                                                         id_salas = m.salas.id_salas,
                                                                         descripcion = m.descripcion,
                                                                         estado = m.estado,
                                                                         usuario_registro = m.usuario_registro,
                                                                         salas = new SalasViewModel
                                                                         {
                                                                             id_salas = n.id_salas,
                                                                             descripcion = n.descripcion,
                                                                             estado = n.estado,
                                                                             usuario_registro = n.usuario_registro
                                                                         }
                                                                     }).FirstOrDefault(),
                                                     juzgado = (from o in c.juzgado
                                                                where o.id_juzgado.Equals(r.id_juzgado)
                                                                select new JuzgadoViewModel
                                                                {
                                                                    descripcion = o.descripcion,
                                                                    estado = o.estado,
                                                                    usuario_registro = o.usuario_registro
                                                                }).FirstOrDefault(),
                                                     descripcion = r.descripcion,
                                                     estado = r.estado,
                                                     usuario_registro = r.usuario_registro,

                                                 }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public CasacionViewModel buscarCasacion(int id)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    CasacionViewModel a = (from r in c.casacion
                                           join y in c.tipo_casacion on r.id_tipo_casacion equals y.id_tipo_casacion
                                           join x in c.juzgado on r.id_juzgado equals x.id_juzgado
                                           join l in c.letrado on r.id_letrado equals l.id_letrado
                                           where r.id_casacion.Equals(id)
                                           select new CasacionViewModel
                                           {
                                               id_casacion = r.id_casacion,
                                               id_juzgado = r.juzgado.id_juzgado,
                                               id_letrado = r.letrado.id_letrado,
                                               id_tipo_casacion = r.tipo_casacion.id_tipo_casacion,
                                               completo = r.completo,
                                               consta_de = r.consta_de,
                                               motivo_demanda = r.motivo_demanda,
                                               nombre_apoderado = r.nombre_apoderado,
                                               nombre_demandado = r.nombre_demandado,
                                               nombre_demandante = r.nombre_demandante,
                                               numero_correlativo = r.numero_correlativo,
                                               numero_registro = r.numero_registro,
                                               procedencia = r.procedencia,
                                               resolucion = r.resolucion,
                                               tipoCasacion = (from m in c.tipo_casacion
                                                               join n in c.salas on m.id_sala equals n.id_salas
                                                               where m.id_tipo_casacion.Equals(r.id_tipo_casacion)
                                                               select new TipoCasacionViewModel
                                                               {
                                                                   id_tipo_casacion = m.id_tipo_casacion,
                                                                   id_salas = m.salas.id_salas,
                                                                   descripcion = m.descripcion,
                                                                   estado = m.estado,
                                                                   usuario_registro = m.usuario_registro,
                                                                   salas = new SalasViewModel
                                                                   {
                                                                       id_salas = n.id_salas,
                                                                       descripcion = n.descripcion,
                                                                       estado = n.estado,
                                                                       usuario_registro = n.usuario_registro
                                                                   }
                                                               }).FirstOrDefault(),
                                               juzgado = (from o in c.juzgado
                                                          where o.id_juzgado.Equals(r.id_juzgado)
                                                          select new JuzgadoViewModel
                                                          {
                                                              descripcion = o.descripcion,
                                                              estado = o.estado,
                                                              usuario_registro = o.usuario_registro
                                                          }).FirstOrDefault(),
                                               descripcion = r.descripcion,
                                               estado = r.estado,
                                               usuario_registro = r.usuario_registro,

                                           }).FirstOrDefault();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public int setCasacion(casacion casacion, EntityState estado) //agregar o modificar
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(casacion).State = estado;
                        c.SaveChanges();
                        return casacion.id_casacion;
                    }
                    else
                    {
                        c.Entry(casacion).State = estado;
                        c.SaveChanges();
                        return 0;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region FECHA-CASACIONES

        public List<TipoFechaViewModel> getFechas(int id_casacion, int id_transaccion)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<TipoFechaViewModel> a = (from r in c.FechasDistinct(id_casacion, id_transaccion)
                                                  select new TipoFechaViewModel
                                                  {
                                                      id_tipo_fecha = r.id_tipo_fecha,
                                                      id_tipo_transaccion = r.id_tipo_transaccion,
                                                      descripcion = r.descripcion,
                                                      estado = r.estado,
                                                      usuario_registro = r.usuario_registro,
                                                      /*tipoTransaccion = new TipoTransaccionViewModel
                                                      {
                                                          id_tipo_transaccion = y.id_tipo_transaccion,
                                                          descripcion = y.descripcion,
                                                          estado = y.estado,
                                                          usuario_registro = y.usuario_registro
                                                      }*/
                                                  }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public List<FechaCasacionViewModel> getFechas(int id_casacion)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<FechaCasacionViewModel> a = (from r in c.fecha_casacion
                                                      where r.id_casacion.Equals(id_casacion)
                                                      select new FechaCasacionViewModel
                                                      {
                                                          id_casacion = r.id_casacion,
                                                          id_fecha_casacion = r.id_fecha_casacion,
                                                          id_tipo_fecha = r.id_tipo_fecha,
                                                          tipoFecha = (from x in c.tipo_fecha
                                                                       where x.id_tipo_fecha.Equals(r.id_tipo_fecha)
                                                                       select new TipoFechaViewModel
                                                                       {
                                                                           id_tipo_fecha = x.id_tipo_fecha,
                                                                           descripcion = x.descripcion,
                                                                           id_tipo_transaccion = x.id_tipo_transaccion,
                                                                           estado = x.estado
                                                                       }).FirstOrDefault(),
                                                          descripcion = r.descripcion,
                                                          estado = r.estado,
                                                          usuario_registro = r.usuario_registro,

                                                      }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public FechaCasacionViewModel buscarFechaCasacion(int id_casacion, int id_fecha)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    FechaCasacionViewModel a = (from r in c.fecha_casacion
                                                where r.id_casacion.Equals(id_casacion) && r.id_tipo_fecha.Equals(id_fecha)
                                                select new FechaCasacionViewModel
                                                {
                                                    id_casacion = r.id_casacion,
                                                    id_fecha_casacion = r.id_fecha_casacion,
                                                    id_tipo_fecha = r.id_tipo_fecha,
                                                    tipoFecha = (from x in c.tipo_fecha
                                                                 where x.id_tipo_fecha.Equals(r.id_tipo_fecha)
                                                                 select new TipoFechaViewModel
                                                                 {
                                                                     id_tipo_fecha = x.id_tipo_fecha,
                                                                     descripcion = x.descripcion,
                                                                     id_tipo_transaccion = x.id_tipo_transaccion,
                                                                     estado = x.estado
                                                                 }).FirstOrDefault(),
                                                    descripcion = r.descripcion,
                                                    estado = r.estado,
                                                    usuario_registro = r.usuario_registro,

                                                }).FirstOrDefault();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public string setFechaCasacion(fecha_casacion fecha, EntityState estado) //agregar o modificar
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(fecha).State = estado;
                        c.SaveChanges();
                        return "Se ingreso correctamente el registro";
                    }
                    else
                    {
                        c.Entry(fecha).State = estado;
                        c.SaveChanges();
                        return "Se modifico correctamente el registro";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region observaciones-casaciones

        public List<TipoObservacionViewModel> getObservaciones(int id_casacion, int id_transaccion)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<TipoObservacionViewModel> a = (from r in c.ObservacionesDistinct(id_casacion, id_transaccion)
                                                        select new TipoObservacionViewModel
                                                        {
                                                            id_tipo_observacion = r.id_tipo_observacion,
                                                            id_tipo_transaccion = r.id_tipo_transaccion,
                                                            descripcion = r.descripcion,
                                                            estado = r.estado,
                                                            usuario_registro = r.usuario_registro,
                                                            /*tipoTransaccion = new TipoTransaccionViewModel
                                                            {
                                                                id_tipo_transaccion = y.id_tipo_transaccion,
                                                                descripcion = y.descripcion,
                                                                estado = y.estado,
                                                                usuario_registro = y.usuario_registro
                                                            }*/
                                                        }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public List<ObservacionCasacionViewModel> getObservaciones(int id_casacion)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<ObservacionCasacionViewModel> a = (from r in c.observacion_casacion
                                                            where r.id_casacion.Equals(id_casacion)
                                                            select new ObservacionCasacionViewModel
                                                            {
                                                                id_casacion = r.id_casacion,
                                                                id_observacion_casacion = r.id_observacion_casacion,
                                                                id_tipo_observacion = r.id_tipo_observacion,
                                                                tipoObservacion = (from x in c.tipo_observacion
                                                                                   where x.id_tipo_observacion.Equals(r.id_tipo_observacion)
                                                                                   select new TipoObservacionViewModel
                                                                                   {
                                                                                       id_tipo_observacion = x.id_tipo_observacion,
                                                                                       descripcion = x.descripcion,
                                                                                       id_tipo_transaccion = x.id_tipo_transaccion,
                                                                                       estado = x.estado
                                                                                   }).FirstOrDefault(),
                                                                descripcion = r.descripcion,
                                                                estado = r.estado,
                                                                usuario_registro = r.usuario_registro,

                                                            }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public string setObservacionCasacion(observacion_casacion obs, EntityState estado) //agregar o modificar
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(obs).State = estado;
                        c.SaveChanges();
                        return "Se ingreso correctamente el registro";
                    }
                    else
                    {
                        c.Entry(obs).State = estado;
                        c.SaveChanges();
                        return "Se modifico correctamente el registro";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        ////////////////////////////AUTENTICAS/////////////////////////////////////
        #region Certificadores
        public List<CertificadorViewModel> getCertificador()
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<CertificadorViewModel> a = (from r in c.certificador
                                                     select new CertificadorViewModel
                                                     {
                                                         estado = r.estado,
                                                         id_certificador = r.id_certificador,
                                                         nombre_certificador = r.nombre_certificador,
                                                         id_puesto = r.id_puesto,
                                                         puesto = (from x in c.puesto
                                                                   where r.id_puesto.Equals(x.id_puesto)
                                                                   select new PuestoViewModel {
                                                                       id_puesto = x.id_puesto,
                                                                       estado = x.estado,
                                                                       fecha_registro = x.fecha_registro,
                                                                       usuario_registro = x.usuario_registro,
                                                                       puesto = x.puesto1
                                                                   }).FirstOrDefault(),
                                                         usuario_registro = r.usuario_registro,
                                                         fecha_registro = r.fecha_registro
                                                     }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public CertificadorViewModel buscarCertificador(int id)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    CertificadorViewModel a = (from r in c.certificador.AsNoTracking()
                                               where r.id_certificador.Equals(id)
                                               select new CertificadorViewModel
                                               {
                                                   nombre_certificador = r.nombre_certificador,
                                                   estado = r.estado,
                                                   usuario_registro = r.usuario_registro,
                                                   puesto = (from x in c.puesto
                                                             where r.id_puesto.Equals(x.id_puesto)
                                                             select new PuestoViewModel
                                                             {
                                                                 id_puesto = x.id_puesto,
                                                                 estado = x.estado,
                                                                 fecha_registro = x.fecha_registro,
                                                                 usuario_registro = x.usuario_registro,
                                                                 puesto = x.puesto1
                                                             }).FirstOrDefault(),
                                                   fecha_registro = r.fecha_registro,
                                                   id_certificador = r.id_certificador
                                               }).FirstOrDefault();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public string setCertificador(certificador certificador, EntityState estado) //agregar o modificar
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(certificador).State = estado;
                        c.SaveChanges();
                        return "Se ingreso correctamente el registro";
                    }
                    else
                    {
                        c.Entry(certificador).State = estado;
                        c.SaveChanges();
                        return "Se modifico correctamente el registro";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Secretarias
        public List<SecretarioViewModel> getSecretario()
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<SecretarioViewModel> a = (from r in c.secretario
                                                   select new SecretarioViewModel
                                                   {
                                                       id_secretario = r.id_secretario,
                                                       nombre = r.nombre,
                                                       puesto = r.puesto,
                                                       usuario_registro = r.usuario_registro,
                                                       fecha_registro = r.fecha_registro,
                                                       estado = r.estado
                                                   }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public SecretarioViewModel buscarSecretario(int id)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    SecretarioViewModel a = (from r in c.secretario.AsNoTracking()
                                             where r.id_secretario.Equals(id)
                                             select new SecretarioViewModel
                                             {
                                                 nombre = r.nombre,
                                                 usuario_registro = r.usuario_registro,
                                                 puesto = r.puesto,
                                                 fecha_registro = r.fecha_registro,
                                                 id_secretario = r.id_secretario,
                                                 estado = r.estado
                                             }).FirstOrDefault();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public string setSecretario(secretario Secretario, EntityState estado) //agregar o modificar
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(Secretario).State = estado;
                        c.SaveChanges();
                        return "Se ingreso correctamente el registro";
                    }
                    else
                    {
                        c.Entry(Secretario).State = estado;
                        c.SaveChanges();
                        return "Se modifico correctamente el registro";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region FECHA-AUTENTICAS
        public List<TipoFechaViewModel> getFechas2(int id_autentica, int id_transaccion)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<TipoFechaViewModel> a = (from r in c.FechasDistinct2(id_autentica, id_transaccion)
                                                  select new TipoFechaViewModel
                                                  {
                                                      id_tipo_fecha = r.id_tipo_fecha,
                                                      id_tipo_transaccion = r.id_tipo_transaccion,
                                                      descripcion = r.descripcion,
                                                      estado = r.estado,
                                                      usuario_registro = r.usuario_registro,
                                                  }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public List<FechaAutenticaViewModel> getFechasAutenticas(int id_autentica)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<FechaAutenticaViewModel> a = (from r in c.fecha_autentica
                                                       where r.id_autentica_recepcion.Equals(id_autentica)
                                                       select new FechaAutenticaViewModel
                                                       {
                                                           id_autentica_recepcion = r.id_autentica_recepcion,
                                                           id_fecha_autentica = r.id_fecha_autentica,
                                                           id_tipo_fecha = r.id_tipo_fecha,
                                                           tipoFecha = (from x in c.tipo_fecha
                                                                        where x.id_tipo_fecha.Equals(r.id_tipo_fecha)
                                                                        select new TipoFechaViewModel
                                                                        {
                                                                            id_tipo_fecha = x.id_tipo_fecha,
                                                                            descripcion = x.descripcion,
                                                                            id_tipo_transaccion = x.id_tipo_transaccion,
                                                                            estado = x.estado
                                                                        }).FirstOrDefault(),
                                                           descripcion = r.descripcion,
                                                           estado = r.estado,
                                                           usuario_registro = r.usuario_registro,

                                                       }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public FechaAutenticaViewModel buscarFechaAutentica(int id_autentica, int id_fecha)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    FechaAutenticaViewModel a = (from r in c.fecha_autentica
                                                 where r.id_autentica_recepcion.Equals(id_autentica) && r.id_tipo_fecha.Equals(id_fecha)
                                                 select new FechaAutenticaViewModel
                                                 {
                                                     id_autentica_recepcion = r.id_autentica_recepcion,
                                                     id_fecha_autentica = r.id_fecha_autentica,
                                                     id_tipo_fecha = r.id_tipo_fecha,
                                                     tipoFecha = (from x in c.tipo_fecha
                                                                  where x.id_tipo_fecha.Equals(r.id_tipo_fecha)
                                                                  select new TipoFechaViewModel
                                                                  {
                                                                      id_tipo_fecha = x.id_tipo_fecha,
                                                                      descripcion = x.descripcion,
                                                                      id_tipo_transaccion = x.id_tipo_transaccion,
                                                                      estado = x.estado
                                                                  }).FirstOrDefault(),
                                                     descripcion = r.descripcion,
                                                     estado = r.estado,
                                                     usuario_registro = r.usuario_registro,

                                                 }).FirstOrDefault();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public string setFechaAutentica(fecha_autentica fecha, EntityState estado) //agregar o modificar
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(fecha).State = estado;
                        c.SaveChanges();
                        return "Se ingreso correctamente el registro";
                    }
                    else
                    {
                        c.Entry(fecha).State = estado;
                        c.SaveChanges();
                        return "Se modifico correctamente el registro";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region observaciones-Autenticas
        public List<TipoObservacionViewModel> getObservaciones2(int id_autentica, int id_transaccion)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<TipoObservacionViewModel> a = (from r in c.ObservacionesDistinct2(id_autentica, id_transaccion)
                                                        select new TipoObservacionViewModel
                                                        {
                                                            id_tipo_observacion = r.id_tipo_observacion,
                                                            id_tipo_transaccion = r.id_tipo_transaccion,
                                                            descripcion = r.descripcion,
                                                            estado = r.estado,
                                                            usuario_registro = r.usuario_registro, 
                                                        }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public List<ObservacionAutenticaViewModel> getObservacionesAutenticas(int id_autentica)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<ObservacionAutenticaViewModel> a = (from r in c.observacion_autentica
                                                             where r.id_autentica_recepcion.Equals(id_autentica)
                                                             select new ObservacionAutenticaViewModel
                                                             {
                                                                 id_autentica_recepcion = r.id_autentica_recepcion,
                                                                 id_observacion_autentica = r.id_observacion_autentica,
                                                                 id_tipo_observacion = r.id_tipo_observacion,
                                                                 tipoObservacion = (from x in c.tipo_observacion
                                                                                    where x.id_tipo_observacion.Equals(r.id_tipo_observacion)
                                                                                    select new TipoObservacionViewModel
                                                                                    {
                                                                                        id_tipo_observacion = x.id_tipo_observacion,
                                                                                        descripcion = x.descripcion,
                                                                                        id_tipo_transaccion = x.id_tipo_transaccion,
                                                                                        estado = x.estado
                                                                                    }).FirstOrDefault(),
                                                                 descripcion = r.descripcion,
                                                                 estado = r.estado,
                                                                 usuario_registro = r.usuario_registro,

                                                             }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public string setObservacionAutentica(observacion_autentica obs, EntityState estado) //agregar o modificar
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(obs).State = estado;
                        c.SaveChanges();
                        return "Se ingreso correctamente el registro";
                    }
                    else
                    {
                        c.Entry(obs).State = estado;
                        c.SaveChanges();
                        return "Se modifico correctamente el registro";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region autentica_recepcion
        public int correlativoAutenticas(string anio)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    int a = (from r in c.autentica_recepcion
                             join n in c.tipo_solicitud on r.id_tipo_solicitud equals n.id_tipo_solicitud
                             where r.fecha_registro.Year.ToString().Equals(anio)
                             select new AutenticaRecepcionViewModel                             
                             {
                                 id_autentica_recepcion = r.id_autentica_recepcion,
                             }).Count();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            //return 0;
        }
        public List<AutenticaRecepcionViewModel> getAutenticas()
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    var x = (from r in c.autentica_recepcion
                             join n in c.tipo_solicitud on r.id_tipo_solicitud equals n.id_tipo_solicitud
                             select new AutenticaRecepcionViewModel
                             {
                                 auto = r.auto,
                                 autorizada= r.autorizada,
                                 denegar = r.denegar,
                                 id_autentica_recepcion = r.id_autentica_recepcion,
                                 id_tipo_solicitud = r.id_tipo_solicitud,
                                 nombre_apoderado = r.nombre_apoderado,
                                 numero_autentica = r.numero_autentica,
                                 nombre_autenticador = r.nombre_autenticador,
                                 numero_recibo = r.numero_recibo,
                                 observacion = r.observacion,
                                 usuario_registro = r.usuario_registro,
                                 tipoSolicitud = new TipoSolicitudViewModel
                                                 {
                                                    descripcion = n.descripcion,
                                                    estado= n.estado,
                                                    id_tipo_solicitud = n.id_tipo_solicitud,
                                                    usuario_registro = n.usuario_registro
                                                 }
                             }).ToList();
                    return x;
                }
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        public AutenticaRecepcionViewModel buscarAutenticaRecepcion( int id)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    var x = (from r in c.autentica_recepcion
                             join n in c.tipo_solicitud on r.id_tipo_solicitud equals n.id_tipo_solicitud
                             where r.id_autentica_recepcion.Equals(id)
                             select new AutenticaRecepcionViewModel
                             {
                                 auto = r.auto,
                                 autorizada = r.autorizada,
                                 denegar = r.denegar,
                                 id_autentica_recepcion = r.id_autentica_recepcion,
                                 id_tipo_solicitud = r.id_tipo_solicitud,
                                 nombre_apoderado = r.nombre_apoderado,
                                 numero_autentica = r.numero_autentica,
                                 nombre_autenticador = r.nombre_autenticador,
                                 numero_recibo = r.numero_recibo,
                                 observacion = r.observacion,
                                 usuario_registro = r.usuario_registro,
                                 tipoSolicitud = new TipoSolicitudViewModel
                                 {
                                     descripcion = n.descripcion,
                                     estado = n.estado,
                                     id_tipo_solicitud = n.id_tipo_solicitud,
                                     usuario_registro = n.usuario_registro
                                 }
                             }).FirstOrDefault();
                    return x;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public int setAutenticas(autentica_recepcion autentica, EntityState estado)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(autentica).State = estado;
                        c.SaveChanges();
                        return autentica.id_autentica_recepcion;
                    }
                    else
                    {
                        c.Entry(autentica).State = estado;
                        c.SaveChanges();
                        return autentica.id_autentica_recepcion;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region autentica_registro
        public List<AutenticaRegistroViewModel> getAutenticaRegistro()
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    var x = (from r in c.autentica_registro
                             join m in c.secretario on r.id_secretario equals m.id_secretario
                             join n in c.certificador on r.id_certificador equals n.id_certificador
                             select new AutenticaRegistroViewModel
                             {
                                id_secretario = r.id_secretario,
                                usuario_registro = r.usuario_registro,
                                numero_autentica = r.numero_autentica,
                                id_certificador = r.id_certificador,
                                id_autentica_registro= r.id_autentica_registro,
                                id_autentica_recepcion = r.id_autentica_recepcion,
                                descripcion = r.descripcion,
                                certificador = new CertificadorViewModel
                                {
                                    id_certificador = r.id_certificador,
                                    usuario_registro = n.usuario_registro,
                                    estado = n.estado,
                                    fecha_registro= n.fecha_registro,
                                    nombre_certificador = n.nombre_certificador,
                                    puesto = (from y in c.puesto
                                              where n.id_puesto.Equals(y.id_puesto)
                                              select new PuestoViewModel
                                              {
                                                  id_puesto = y.id_puesto,
                                                  estado = y.estado,
                                                  fecha_registro = y.fecha_registro,
                                                  usuario_registro = y.usuario_registro,
                                                  puesto = y.puesto1
                                              }).FirstOrDefault(),
                                },
                                secretario = new SecretarioViewModel
                                {
                                    id_secretario = r.id_secretario,
                                    usuario_registro = m.usuario_registro,
                                    estado = m.estado,
                                    fecha_registro = m.fecha_registro,
                                    nombre = m.nombre,
                                    puesto = m.puesto
                                },
                                autenticaRecepcion = (from o in c.autentica_recepcion
                                                      join p in c.tipo_solicitud on o.id_tipo_solicitud equals p.id_tipo_solicitud
                                                      where o.id_autentica_recepcion.Equals(r.id_autentica_recepcion)
                                                      select new AutenticaRecepcionViewModel
                                                      {
                                                          auto = o.auto,
                                                          autorizada = o.autorizada,
                                                          denegar = o.denegar,
                                                          id_autentica_recepcion = o.id_autentica_recepcion,
                                                          id_tipo_solicitud = o.id_tipo_solicitud,
                                                          nombre_apoderado = o.nombre_apoderado,
                                                          numero_autentica = o.numero_autentica,
                                                          nombre_autenticador = o.nombre_autenticador,
                                                          numero_recibo = o.numero_recibo,
                                                          observacion = o.observacion,
                                                          usuario_registro = r.usuario_registro,
                                                          tipoSolicitud = new TipoSolicitudViewModel
                                                          {
                                                              descripcion = p.descripcion,
                                                              estado = p.estado,
                                                              id_tipo_solicitud = p.id_tipo_solicitud,
                                                              usuario_registro = p.usuario_registro
                                                          }
                                                      }).FirstOrDefault()

                             }).ToList();
                    return x;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public AutenticaRegistroViewModel buscarAutenticaRegistro(int id)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    var x = (from r in c.autentica_registro
                            join m in c.secretario on r.id_secretario equals m.id_secretario
                            join n in c.certificador on r.id_certificador equals n.id_certificador
                            where r.id_autentica_registro.Equals(id)
                            select new AutenticaRegistroViewModel
                            {
                                id_secretario = r.id_secretario,
                                usuario_registro = r.usuario_registro,
                                numero_autentica = r.numero_autentica,
                                id_certificador = r.id_certificador,
                                id_autentica_registro = r.id_autentica_registro,
                                id_autentica_recepcion = r.id_autentica_recepcion,
                                descripcion = r.descripcion,
                                certificador = new CertificadorViewModel
                                {
                                    id_certificador = r.id_certificador,
                                    usuario_registro = n.usuario_registro,
                                    estado = n.estado,
                                    fecha_registro = n.fecha_registro,
                                    nombre_certificador = n.nombre_certificador,
                                    puesto = (from y in c.puesto
                                              where n.id_puesto.Equals(y.id_puesto)
                                              select new PuestoViewModel
                                              {
                                                  id_puesto = y.id_puesto,
                                                  estado = y.estado,
                                                  fecha_registro = y.fecha_registro,
                                                  usuario_registro = y.usuario_registro,
                                                  puesto = y.puesto1
                                              }).FirstOrDefault(),
                                },
                                secretario = new SecretarioViewModel
                                {
                                    id_secretario = r.id_secretario,
                                    usuario_registro = m.usuario_registro,
                                    estado = m.estado,
                                    fecha_registro = m.fecha_registro,
                                    nombre = m.nombre,
                                    puesto = m.puesto
                                },
                                autenticaRecepcion = (from o in c.autentica_recepcion
                                                    join p in c.tipo_solicitud on o.id_tipo_solicitud equals p.id_tipo_solicitud
                                                    where o.id_autentica_recepcion.Equals(r.id_autentica_recepcion)
                                                    select new AutenticaRecepcionViewModel
                                                    {
                                                        auto = o.auto,
                                                        autorizada = o.autorizada,
                                                        denegar = o.denegar,
                                                        id_autentica_recepcion = o.id_autentica_recepcion,
                                                        id_tipo_solicitud = o.id_tipo_solicitud,
                                                        nombre_apoderado = o.nombre_apoderado,
                                                        numero_autentica = o.numero_autentica,
                                                        nombre_autenticador = o.nombre_autenticador,
                                                        numero_recibo = o.numero_recibo,
                                                        observacion = o.observacion,
                                                        usuario_registro = r.usuario_registro,
                                                        tipoSolicitud = new TipoSolicitudViewModel
                                                        {
                                                            descripcion = p.descripcion,
                                                            estado = p.estado,
                                                            id_tipo_solicitud = p.id_tipo_solicitud,
                                                            usuario_registro = p.usuario_registro
                                                        }
                                                    }).FirstOrDefault()

                            }).FirstOrDefault();
                    return x;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public int setAutenticaRegistro(autentica_registro autentica, EntityState estado)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(autentica).State = estado;
                        c.SaveChanges();
                        return autentica.id_autentica_recepcion;
                    }
                    else
                    {
                        c.Entry(autentica).State = estado;
                        c.SaveChanges();
                        return autentica.id_autentica_recepcion;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        /////////////////////// SEGURIDAD

        #region modulos

        public List<TreeViewModel> getTree()
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<TreeViewModel> a = (from r in c.modulo
                                             select new TreeViewModel
                                             {

                                                 text = r.nombre,
                                                 items = (from m in c.opcionModulo
                                                          where m.id_modulo.Equals(r.id_modulo)
                                                          select new ChildViewModel
                                                          {
                                                              id = m.id_opcionModulo,
                                                              text = m.nombre,
                                                          }).ToList(),
                                             }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public List<ModuloViewModel> getAllModulos()
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<ModuloViewModel> a = (from r in c.modulo
                                               select new ModuloViewModel
                                               {
                                                   id_modulo = r.id_modulo,
                                                   nombre = r.nombre,
                                                   enlace = r.enlace,
                                                   b_estado = r.b_estado,
                                                   opciones = (from m in c.opcionModulo
                                                               where m.id_modulo.Equals(r.id_modulo)
                                                                select new OpcionModuloViewModel
                                                                {
                                                                    id_opcionModulo = m.id_opcionModulo,
                                                                    id_modulo = m.id_modulo,
                                                                    enlace = m.enlace,
                                                                    nombre = m.nombre,
                                                                    descripcion = m.descripcion,
                                                                    fecha_registro = r.fecha_registro,
                                                                }).ToList(),
                                                   fecha_registro = r.fecha_registro,
                                               }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public List<ModuloViewModel> getModulos()
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<ModuloViewModel> a = (from r in c.modulo
                                              select new ModuloViewModel
                                              {
                                                  id_modulo = r.id_modulo,
                                                  nombre = r.nombre,
                                                  enlace = r.enlace,
                                                  b_estado = r.b_estado,
                                                  fecha_registro = r.fecha_registro,
                                              }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public ModuloViewModel buscarModulo(int id)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    ModuloViewModel a = (from r in c.modulo
                                        where r.id_modulo.Equals(id)
                                        select new ModuloViewModel
                                        {
                                            id_modulo = r.id_modulo,
                                            nombre = r.nombre,
                                            enlace = r.enlace,
                                            b_estado = r.b_estado,
                                            fecha_registro = r.fecha_registro,
                                        }).FirstOrDefault();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public string setModulo(modulo mod, EntityState estado) //agregar o modificar
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(mod).State = estado;
                        c.SaveChanges();
                        return "Se ingreso correctamente el registro";
                    }
                    else
                    {
                        c.Entry(mod).State = estado;
                        c.SaveChanges();
                        return "Se modifico correctamente el registro";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region opcionModulo
        public List<OpcionModuloViewModel> getOpcionModulos()
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<OpcionModuloViewModel> a = (from r in c.opcionModulo
                                               select new OpcionModuloViewModel
                                               {
                                                   id_opcionModulo = r.id_opcionModulo,
                                                   id_modulo = r.id_modulo,
                                                   enlace = r.enlace,
                                                   nombre = r.nombre,
                                                   descripcion = r.descripcion,
                                                   modulo = (from x in c.modulo
                                                             where x.id_modulo.Equals(r.id_modulo)
                                                             select new ModuloViewModel
                                                             {
                                                                 id_modulo = x.id_modulo,
                                                                 nombre = x.nombre,
                                                                 b_estado = x.b_estado,
                                                                 fecha_registro = x.fecha_registro,
                                                             }).FirstOrDefault(),
                                                   fecha_registro = r.fecha_registro,
                                               }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public OpcionModuloViewModel buscarOpcionModulo(int id)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    OpcionModuloViewModel a = (from r in c.opcionModulo
                                               where r.id_opcionModulo.Equals(id)
                                         select new OpcionModuloViewModel
                                         {
                                             id_opcionModulo = r.id_opcionModulo,
                                             id_modulo = r.id_modulo,
                                             nombre = r.nombre,
                                             enlace = r.enlace,
                                             descripcion = r.descripcion,
                                             modulo = (from x in c.modulo
                                                       where x.id_modulo.Equals(r.id_modulo)
                                                       select new ModuloViewModel
                                                       {
                                                           id_modulo = x.id_modulo,
                                                           nombre = x.nombre,
                                                           b_estado = x.b_estado,
                                                           fecha_registro = x.fecha_registro,
                                                       }).FirstOrDefault(),
                                             fecha_registro = r.fecha_registro,
                                         }).FirstOrDefault();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public string setOpcionModulo(opcionModulo mod, EntityState estado) //agregar o modificar
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(mod).State = estado;
                        c.SaveChanges();
                        return "Se ingreso correctamente el registro";
                    }
                    else
                    {
                        c.Entry(mod).State = estado;
                        c.SaveChanges();
                        return "Se modifico correctamente el registro";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region role
        public List<RoleViewModel> getRole()
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<RoleViewModel> a = (from r in c.role
                                               select new RoleViewModel
                                               {
                                                   id_role = r.id_role,
                                                   nombre = r.nombre,
                                                   descripcion = r.descripcion,
                                                   b_estado = r.b_estado,
                                                   fecha_registro = r.fecha_registro,
                                               }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public RoleViewModel buscarRole(int id)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    RoleViewModel a = (from r in c.role
                                         where r.id_role.Equals(id)
                                         select new RoleViewModel
                                         {
                                             id_role = r.id_role,
                                             nombre = r.nombre,
                                             descripcion = r.descripcion,
                                             b_estado = r.b_estado,
                                             fecha_registro = r.fecha_registro,
                                         }).FirstOrDefault();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public string setRole(role role, EntityState estado) //agregar o modificar
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(role).State = estado;
                        c.SaveChanges();
                        return "Se ingreso correctamente el registro";
                    }
                    else
                    {
                        c.Entry(role).State = estado;
                        c.SaveChanges();
                        return "Se modifico correctamente el registro";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region usuario
        public List<UsuarioViewModel> getUsuarios()
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<UsuarioViewModel> a = (from r in c.usuario
                                               select new UsuarioViewModel
                                               {
                                                   id_usuario = r.id_usuario,
                                                   nombre_completo = r.nombre_usuario,
                                                   correo = r.correo,
                                                   nombre_usuario = r.nombre_usuario,
                                                   contrasena = r.contrasena,
                                                   id_role = r.id_role,
                                                   role = (from x in c.role
                                                           where x.id_role.Equals(r.id_role)
                                                           select new RoleViewModel
                                                           {
                                                               id_role = x.id_role,
                                                               nombre = x.nombre,
                                                               descripcion = x.descripcion,
                                                               b_estado = x.b_estado,
                                                               fecha_registro = x.fecha_registro,
                                                           }).FirstOrDefault(),
                                                   b_estado = r.b_estado,
                                                   f_registro = r.f_registro,
                                               }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public UsuarioViewModel buscarUsuario(int id)
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    UsuarioViewModel a = (from r in c.usuario
                                          where r.id_usuario.Equals(id)
                                          select new UsuarioViewModel
                                          {
                                              id_usuario = r.id_usuario,
                                              nombre_completo = r.nombre_usuario,
                                              correo = r.correo,
                                              nombre_usuario = r.nombre_usuario,
                                              contrasena = r.contrasena,
                                              id_role = r.id_role,
                                              role = (from x in c.role
                                                      where x.id_role.Equals(r.id_role)
                                                      select new RoleViewModel
                                                      {
                                                          id_role = x.id_role,
                                                          nombre = x.nombre,
                                                          descripcion = x.descripcion,
                                                          b_estado = x.b_estado,
                                                          fecha_registro = x.fecha_registro,
                                                      }).FirstOrDefault(),
                                              b_estado = r.b_estado,
                                              f_registro = r.f_registro,
                                          }).FirstOrDefault();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public string setUsuario(usuario u, EntityState estado) //agregar o modificar
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(u).State = estado;
                        c.SaveChanges();
                        return "Se ingreso correctamente el registro";
                    }
                    else
                    {
                        c.Entry(u).State = estado;
                        c.SaveChanges();
                        return "Se modifico correctamente el registro";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region log
        public List<LogViewModel> getLogs()
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<LogViewModel> a = (from r in c.log
                                            select new LogViewModel
                                            {
                                                id_opcionModulo = r.id_opcionModulo,
                                                id_log = r.id_log,
                                                descripcion = r.descripcion,
                                                id_usuario = r.id_usuario,
                                                opcionModulo = (from m in c.opcionModulo
                                                                where m.id_opcionModulo.Equals(r.id_opcionModulo)
                                                                select new OpcionModuloViewModel
                                                                {
                                                                    id_opcionModulo = m.id_opcionModulo,
                                                                    id_modulo = m.id_modulo,
                                                                    nombre = m.nombre,
                                                                    descripcion = m.descripcion,
                                                                    modulo = (from x in c.modulo
                                                                            where x.id_modulo.Equals(m.id_modulo)
                                                                            select new ModuloViewModel
                                                                            {
                                                                                id_modulo = x.id_modulo,
                                                                                nombre = x.nombre,
                                                                                b_estado = x.b_estado,
                                                                                fecha_registro = x.fecha_registro,
                                                                            }).FirstOrDefault(),
                                                                    fecha_registro = m.fecha_registro,
                                                                }).FirstOrDefault(),
                                                usuario = (from u in c.usuario
                                                        where u.id_usuario.Equals(r.id_usuario)
                                                        select new UsuarioViewModel
                                                        {
                                                            id_usuario = u.id_usuario,
                                                            nombre_completo = u.nombre_usuario,
                                                            correo = u.correo,
                                                            nombre_usuario = u.nombre_usuario,
                                                            contrasena = u.contrasena,
                                                            id_role = u.id_role,
                                                            role = (from x in c.role
                                                                    where x.id_role.Equals(u.id_role)
                                                                    select new RoleViewModel
                                                                    {
                                                                        id_role = x.id_role,
                                                                        nombre = x.nombre,
                                                                        descripcion = x.descripcion,
                                                                        b_estado = x.b_estado,
                                                                        fecha_registro = x.fecha_registro,
                                                                    }).FirstOrDefault(),
                                                            b_estado = u.b_estado,
                                                            f_registro = u.f_registro,
                                                        }).FirstOrDefault(),
                                                fecha_registro = r.fecha_registro,
                                            }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public LogViewModel buscarLog(int id) 
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    LogViewModel a = (from r in c.log
                                      where r.id_log.Equals(id)
                                    select new LogViewModel
                                    {
                                        id_opcionModulo = r.id_opcionModulo,
                                        id_log = r.id_log,
                                        descripcion = r.descripcion,
                                        id_usuario = r.id_usuario,
                                        opcionModulo = (from m in c.opcionModulo
                                                        where m.id_opcionModulo.Equals(r.id_opcionModulo)
                                                        select new OpcionModuloViewModel
                                                        {
                                                            id_opcionModulo = m.id_opcionModulo,
                                                            id_modulo = m.id_modulo,
                                                            nombre = m.nombre,
                                                            descripcion = m.descripcion,
                                                            modulo = (from x in c.modulo
                                                                        where x.id_modulo.Equals(m.id_modulo)
                                                                        select new ModuloViewModel
                                                                        {
                                                                            id_modulo = x.id_modulo,
                                                                            nombre = x.nombre,
                                                                            b_estado = x.b_estado,
                                                                            fecha_registro = x.fecha_registro,
                                                                        }).FirstOrDefault(),
                                                            fecha_registro = m.fecha_registro,
                                                        }).FirstOrDefault(),
                                        usuario = (from u in c.usuario
                                                    where u.id_usuario.Equals(r.id_usuario)
                                                    select new UsuarioViewModel
                                                    {
                                                        id_usuario = u.id_usuario,
                                                        nombre_completo = u.nombre_usuario,
                                                        correo = u.correo,
                                                        nombre_usuario = u.nombre_usuario,
                                                        contrasena = u.contrasena,
                                                        id_role = u.id_role,
                                                        role = (from x in c.role
                                                                where x.id_role.Equals(u.id_role)
                                                                select new RoleViewModel
                                                                {
                                                                    id_role = x.id_role,
                                                                    nombre = x.nombre,
                                                                    descripcion = x.descripcion,
                                                                    b_estado = x.b_estado,
                                                                    fecha_registro = x.fecha_registro,
                                                                }).FirstOrDefault(),
                                                        b_estado = u.b_estado,
                                                        f_registro = u.f_registro,
                                                    }).FirstOrDefault(),
                                        fecha_registro = r.fecha_registro,
                                    }).FirstOrDefault();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public string setLog(log log, EntityState estado) //agregar o modificar
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(log).State = estado;
                        c.SaveChanges();
                        return "Se ingreso correctamente el registro";
                    }
                    else
                    {
                        c.Entry(log).State = estado;
                        c.SaveChanges();
                        return "Se modifico correctamente el registro";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region permisos
        public List<PermisosViewModel> getPermisos()
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    List<PermisosViewModel> a = (from r in c.permisos
                                            select new PermisosViewModel
                                            {
                                                id_opcionModulo = r.id_opcionModulo,
                                                id_permisos = r.id_permisos,
                                                id_role = r.id_role,
                                                opcionModulo = (from m in c.opcionModulo
                                                                where m.id_opcionModulo.Equals(r.id_opcionModulo)
                                                                select new OpcionModuloViewModel
                                                                {
                                                                    id_opcionModulo = m.id_opcionModulo,
                                                                    id_modulo = m.id_modulo,
                                                                    nombre = m.nombre,
                                                                    descripcion = m.descripcion,
                                                                    modulo = (from x in c.modulo
                                                                              where x.id_modulo.Equals(m.id_modulo)
                                                                              select new ModuloViewModel
                                                                              {
                                                                                  id_modulo = x.id_modulo,
                                                                                  nombre = x.nombre,
                                                                                  b_estado = x.b_estado,
                                                                                  fecha_registro = x.fecha_registro,
                                                                              }).FirstOrDefault(),
                                                                    fecha_registro = m.fecha_registro,
                                                                }).FirstOrDefault(),
                                                role = (from x in c.role
                                                        where x.id_role.Equals(r.id_role)
                                                        select new RoleViewModel
                                                        {
                                                            id_role = x.id_role,
                                                            nombre = x.nombre,
                                                            descripcion = x.descripcion,
                                                            b_estado = x.b_estado,
                                                            fecha_registro = x.fecha_registro,
                                                        }).FirstOrDefault(),

                                            }).ToList();
                    return a;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public string setPermiso(permisos permiso, EntityState estado) //agregar o modificar
        {
            try
            {
                using (secretariaEntities c = new secretariaEntities())
                {
                    if (estado == EntityState.Added)
                    {
                        c.Entry(permiso).State = estado;
                        c.SaveChanges();
                        return "Se ingreso correctamente el registro";
                    }
                    else
                    {
                        c.Entry(permiso).State = estado;
                        c.SaveChanges();
                        return "Se modifico correctamente el registro";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion
    }
}
