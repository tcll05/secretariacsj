﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecretariaConnection.Models
{
    public class ChildViewModel
    {
        public int id { set; get; }
        public string text { set; get; }
    }
}
