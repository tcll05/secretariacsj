﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecretariaConnection.Models
{
    public class PuestoViewModel
    {
        public int id_puesto { set; get; }
        public string puesto { set; get; }
        public bool estado { set; get; }
        public string usuario_registro { set; get; }
        public DateTime fecha_registro { set; get; }
    }
}
