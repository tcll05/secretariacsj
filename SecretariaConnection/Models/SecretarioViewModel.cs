﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecretariaConnection.Models
{
    public class SecretarioViewModel
    {
        public int id_secretario { set; get; }
        public string nombre { set; get; }
        public string puesto { set; get; }
        public string usuario_registro { set; get; }
        public DateTime fecha_registro { set; get; }
        public bool estado { set; get; }

    }
}