﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecretariaConnection.Models
{
    public class AutenticaRegistroViewModel
    {
        public int id_autentica_registro { set; get; }
        public string numero_autentica { set; get; }
        public string descripcion { set; get; }
        public string usuario_registro { set; get; }
        public int id_autentica_recepcion { set; get; }
        public int id_secretario { set; get; }
        public int id_certificador { set; get; }
        public AutenticaRecepcionViewModel autenticaRecepcion { set; get; }
        public SecretarioViewModel secretario { set; get; }
        public CertificadorViewModel certificador { set; get; }

    }
}