﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecretariaConnection.Models
{
    public class PermisosViewModel
    {
        public int id_permisos { set; get; }
        public int id_role { set; get; }
        public int id_opcionModulo { set; get; }
        public RoleViewModel role { set; get; }
        public OpcionModuloViewModel opcionModulo { set; get; }
    }
}
