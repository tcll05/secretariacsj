﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecretariaConnection.Models
{
    public class OpcionModuloViewModel
    {
        public int id_opcionModulo { set; get; }
        public int id_modulo { set; get; }
        public string nombre { set; get; }
        public string descripcion { set; get; }
        public string enlace { set; get; }
        public DateTime fecha_registro { set; get; }
        public ModuloViewModel modulo { set; get; }

    }
}
