﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecretariaConnection.Models
{
    public class FechaAutenticaViewModel
    {
        public int id_fecha_autentica { set; get; }
        public string descripcion { set; get; }
        public bool estado { set; get; }
        public string usuario_registro { set; get; }
        public int id_autentica_recepcion { set; get; }
        public int id_tipo_fecha { set; get; }
        public AutenticaRecepcionViewModel autenticaRecepcion { set; get; }
        public TipoFechaViewModel tipoFecha { set; get; }
    }
}