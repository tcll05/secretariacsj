﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecretariaConnection.Models
{
    public class TipoCasacionViewModel
    {
        public int id_tipo_casacion { set; get; }
        public string descripcion { set; get; }
        public bool estado { set; get; }
        public string usuario_registro { set; get; }
        public int id_salas { set; get; }
        public SalasViewModel salas { set; get; }
    }
}