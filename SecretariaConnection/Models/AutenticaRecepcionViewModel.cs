﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecretariaConnection.Models
{
    public class AutenticaRecepcionViewModel
    {
        public int id_autentica_recepcion { set; get; }
        public string numero_recibo { set; get; }
        public string numero_autentica { set; get; }
        public string nombre_apoderado { set; get; }
        public string nombre_autenticador { set; get; }
        public string denegar { set; get; }
        public string auto { set; get; }
        public string autorizada { set; get; }
        public string observacion { set; get; }
        public string usuario_registro { set; get; }
        public int id_tipo_solicitud { set; get; }
        public TipoSolicitudViewModel tipoSolicitud { set; get; }
        public List<ObservacionAutenticaViewModel> observacionAutenticas { set; get; } = new List<ObservacionAutenticaViewModel>();
        public List<FechaAutenticaViewModel> fechaAutenticas { set; get; } = new List<FechaAutenticaViewModel>();

    }
}