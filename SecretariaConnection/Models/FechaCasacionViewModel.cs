﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecretariaConnection.Models
{
    public class FechaCasacionViewModel
    {
        public int id_fecha_casacion { set; get; }
        public string descripcion { set; get; }
        public bool estado { set; get; }
        public string usuario_registro { set; get; }
        public int id_casacion { set; get; }
        public int id_tipo_fecha { set; get; }
        public CasacionViewModel casacion { set; get; }
        public TipoFechaViewModel tipoFecha { set; get; }
    }
}