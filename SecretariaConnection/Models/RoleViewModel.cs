﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecretariaConnection.Models
{
    public class RoleViewModel
    {
        public int id_role { set; get; }
        public string nombre { set; get; }
        public string descripcion { set; get; }
        public DateTime fecha_registro { set; get; }
        public bool b_estado { set; get; }
    }
}
