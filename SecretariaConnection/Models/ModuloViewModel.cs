﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecretariaConnection.Models
{
    public class ModuloViewModel
    {
        public int id_modulo { get; set; }
        public string nombre { get; set; }
        public bool b_estado { get; set; }
        public string enlace { set; get; }
        public DateTime fecha_registro { set; get; }
        public List<OpcionModuloViewModel> opciones { set; get; }
    }
}
