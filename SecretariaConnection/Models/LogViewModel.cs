﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecretariaConnection.Models
{
    public class LogViewModel
    {
        public int id_log { set; get; }
        public string descripcion { set; get; }
        public int id_usuario { set; get; }
        public int id_opcionModulo { set; get; }
        public DateTime fecha_registro { set; get; }
        public UsuarioViewModel usuario { set; get; }
        public OpcionModuloViewModel opcionModulo { set; get; }
    }
}
