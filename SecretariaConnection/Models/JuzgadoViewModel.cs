﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecretariaConnection.Models
{
    public class JuzgadoViewModel
    {
        public int id_juzgado { set; get; }
        public string descripcion { set; get; }
        public bool estado { set; get; }
        public string usuario_registro { set; get; }
    }
}