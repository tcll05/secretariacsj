﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecretariaConnection.Models
{
    public class CertificadorViewModel
    {
        public int id_certificador { set; get; }
        public string nombre_certificador { set; get; }
        public int id_puesto { set; get; }
        public bool estado { set; get; }
        public string usuario_registro { set; get; }
        public DateTime fecha_registro { set; get; }
        public PuestoViewModel puesto { set; get; }
    }
}