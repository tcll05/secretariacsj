﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecretariaConnection.Models
{
    public class UsuarioViewModel
    {
        public int id_usuario { set; get; }
        public string nombre_completo { set; get; }
        public string correo { set; get; }
        public string nombre_usuario { set; get; }
        public string contrasena { set; get; }
        public int id_role { set; get; }
        public bool b_estado { set; get; }
        public DateTime f_registro { set; get; }
        public RoleViewModel role { set; get; }
    }
}
