﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecretariaConnection.Models
{
    public class TreeViewModel
    {
        public string text { set; get; }
        public List<ChildViewModel> items { set; get; }
    }
}
