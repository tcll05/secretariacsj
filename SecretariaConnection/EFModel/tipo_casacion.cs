//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SecretariaConnection.EFModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class tipo_casacion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tipo_casacion()
        {
            this.casacion = new HashSet<casacion>();
        }
    
        public int id_tipo_casacion { get; set; }
        public int id_sala { get; set; }
        public string descripcion { get; set; }
        public bool estado { get; set; }
        public string usuario_registro { get; set; }
        public System.DateTime fecha_registro { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<casacion> casacion { get; set; }
        public virtual salas salas { get; set; }
    }
}
