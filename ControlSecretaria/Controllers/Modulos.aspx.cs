﻿using SecretariaConnection;
using SecretariaConnection.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ControlSecretaria.Controllers
{
    public partial class Modulos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static List<ModuloViewModel> listaModulos()
        {
            Repositorio repo = new Repositorio();
            var ds = repo.getAllModulos();
            return ds;
        }
    }
}