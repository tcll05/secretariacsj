﻿function setRow(row, name) {
    var rowData = row.parentNode.parentNode;
    var index = rowData.rowIndex;
    document.getElementById('MainContent_hdnIndex').value = name + ";" + index;
}
function eliminar(row) {
    var rowData = row.parentNode.parentNode;
    var index = rowData.rowIndex;
    var x = confirm('¿Eliminar Registro?');
    if (x) {
        setRow(row, 'eliminar');
        return true;
    }
    return false;
}
