﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FormPermisos.aspx.cs" Inherits="ControlSecretaria.Componentes.Seguridad.FormPermisos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../../Scripts/all.js"></script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Permisos</h1>
    <div class="card">

        <div class="card-body" style="padding-top: 0px; padding-bottom: 0px;">
            <br />
            <div class="form-group row">
                <div class="col-md-1">
                    <h4>Role</h4>
                </div>
                <div class="col-md-11">
                    <asp:DropDownList ID="cmbRole" runat="server" CssClass="form-control" placeholder="Role"></asp:DropDownList>
                </div>
                <small id="requirement8" runat="server" visible="false" class="form-text text-muted">
                    <label id="RequiredFieldValidator1">Campo Requerido</label>
                </small>
            </div>
            <hr />
            <div id="tv">
                <div id="treeview-checkbox-demo">
                    <ul>
                        <li v-for="modulo in modulos">&nbsp {{modulo.nombre}}
                            <ul v-if="modulo.opciones">
                                <li v-for="opcion in modulo.opciones" v-bind:data-value="opcion.nombre">&nbsp {{opcion.nombre}}
                                    <%--<input class="form-check-input" v-bind:id="'chk'+opcion.id" v-bind:value="opcion.nombre" type="checkbox" v-model="checkedModulos">
                                    <label class="form-check-label" for="'chk'+opcion.id">{{opcion.nombre}}</label>--%>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <button type="button" class="btn btn-success" id="show-values">Get Values</button>
        <span id="values"></span>
            </div>

            <%--<div id="treeview"></div>--%>
        </div>
    </div>
    <br />
    <%--<div id="vueTests2">
        <div class="accordion" id="accordionExample">
            <div class="card" v-for="modulo in modulos">
                <div class="card-header" v-bind:id="'heading'+modulo.id_modulo">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" v-bind:data-target="'#collapse'+modulo.id_modulo" aria-expanded="true" aria-controls="collapseOne">
                            {{modulo.nombre}}
                        </button>
                    </h2>
                </div>

                <div v-bind:id="'collapse'+modulo.id_modulo" class="collapse show" v-bind:aria-labelledby="'heading'+modulo.id_modulo" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="form-group" v-for="opcion in modulo.opciones">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" v-bind:id="'gridCheck'+opcion.id_opcionModulo">
                                <label class="form-check-label" v-bind:for="'gridCheck'+opcion.id_opcionModulo">
                                    {{opcion.nombre}}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>--%>
    <script src="../../Content/logger.js"></script>
    <script src="../../Content/treeview.js"></script>
    <script type="text/javascript">
        $.ajax({
                type: "GET",
                url: "http://localhost:35451/Controllers/Modulos.aspx/listaModulos",
                data: '',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var example1 = new Vue({
                        el: '#tv',
                        data: {
                            modulos: data.d,
                            checkedModulos: []
                        }
                    });
                    tv();
                },
                error: function (data) { console.log(data) }
        });
        function tv() {
            var x = "";
            $('#treeview-checkbox-demo').treeview({
                debug : true,
                data : []
            });
            $('#show-values').on('click', function () { 
                x = $('#treeview-checkbox-demo').treeview('selectedValues');
                
                $('#values').text(
                    $('#treeview-checkbox-demo').treeview('selectedValues')
                );
                var m = $('#values').text();
                console.log(m);
                var n = stringToJson(m);
                console.log(n);
                var c = JSON.stringify(n);
                console.log(c);
                setPermisos(c);
            });
            //$("#treeview").shieldTreeView();
        }

        function stringToJson(msg) {
            var x = msg.split(",");
            var json = "";
            for (i = 0; i < x.length; i++) {
                if (i == 0)
                    json += '[';
                
                json += '{"nombre":"' + x[i] + '"},';
                if (i == x.length-1)
                    json += ']';
            }
            return json; //386

        }

        function setPermisos(p) {
            $.ajax({
              type: "POST",
              url: "http://localhost:35451/Componentes/Seguridad/FormPermisos.aspx/setPermisos",
              data: {permisos: p},
              contentType: "application/json; charset=utf-8",
              dataType: "text",
              success: function(msg) {
                  console.log(msg);
                },
                error: function (data) { console.log(data) }
            });
        }
        
       </script>
</asp:Content>
