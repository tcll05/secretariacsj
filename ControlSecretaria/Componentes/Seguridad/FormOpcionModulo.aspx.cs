﻿using SecretariaConnection;
using SecretariaConnection.EFModel;
using SecretariaConnection.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ControlSecretaria.Componentes.Seguridad
{
    public partial class FormOpcionModulo : System.Web.UI.Page
    {
        Repositorio conexion = new Repositorio();
        private DataTable dt;
        public enum MessageType { Correcto, Error, Informacion, Advertencia };

        protected void ShowMessage(string Message, object type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "toastrs('" + Message + "','" + type + "');", true);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                dt = new DataTable();
                llenarGrid();
                llenarCombo();
            }
        }

        public void llenarCombo()
        {
            try
            {
                //txtId.Text = "0";
                var ds = conexion.getModulos();
                cmbModulo.DataSource = ds;
                cmbModulo.DataValueField = "id_modulo";
                cmbModulo.DataTextField = "nombre";
                cmbModulo.DataBind();
                cmbModulo.Items.Insert(0, new ListItem("Seleccione un Modulo", "0"));
                //comboTribunal.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, MessageType.Error);
            }
        }

        public void llenarGrid()
        {
            try
            {
                dt.Columns.AddRange(new DataColumn[] {
                             new DataColumn("id_opcionModulo"),
                             new DataColumn("id_modulo")
                            ,new DataColumn("modulo")
                            ,new DataColumn("opcion")
                            ,new DataColumn("descripcion")
                            ,new DataColumn("enlace")
                        });

                var ds = conexion.getOpcionModulos();
                foreach (OpcionModuloViewModel lista in ds)
                {
                    dt.Rows.Add(lista.id_opcionModulo, lista.id_modulo, lista.modulo.nombre, lista.nombre, lista.descripcion, lista.enlace);
                }
                grdvSalas.DataSource = dt;
                grdvSalas.DataBind();
                grdvSalas.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            catch (Exception ex)
            {
                //ShowMessage(ex.Message, MessageType.Error);
            }
        }

        protected void grdvSalas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dt = new DataTable();
            llenarGrid();

            grdvSalas.PageIndex = e.NewPageIndex;
            DataBind();
            panelGrid.Update();
        }

        protected void hdnIndex_TextChanged(object sender, EventArgs e)
        {
            string[] s = hdnIndex.Text.ToString().Split(';');
            if (s[0].Equals("editar"))
                btnEdit_Click(sender, e);
            else
                btnDelete_Click(sender, e);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            requirement8.Visible = false;
            string[] s = hdnIndex.Text.Split(';');
            int index = Convert.ToInt32(s[1]);
            GridViewRow row = grdvSalas.Rows[index - 1];
            //row.FindControl("tribunal").Visible = true;
            txtCodigo.Text = (row.FindControl("id_opcionModulo") as HiddenField).Value;
            txtNombre.Text = (row.FindControl("opcion") as Label).Text;
            cmbModulo.SelectedValue = (row.FindControl("id_modulo") as HiddenField).Value;
            txtDescripcion.Text = (row.FindControl("descripcion") as Label).Text;
            txtEnlace.Text = (row.FindControl("enlace") as Label).Text;
            btnUpdate.Text = "Modificar";
        }

        public bool validarBtn(string msg)
        {
            if (msg.Equals("Modificar")) return true; else return false;
        }

        public bool validarCmb()
        {
            if (cmbModulo.SelectedIndex > 0)
            {
                return true;
            }
            else return false;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!validarCmb())
            {
                requirement8.Visible = true;
            }
            else
            {
                opcionModulo opcion = new opcionModulo()
                {
                    descripcion = txtDescripcion.Text,
                    id_modulo = Convert.ToInt32(cmbModulo.SelectedValue),
                    fecha_registro = DateTime.Now,
                    enlace = txtEnlace.Text,
                    nombre = txtNombre.Text,
                };
                if (validarBtn(btnUpdate.Text))
                {
                    opcion.id_opcionModulo = Convert.ToInt32(txtCodigo.Text);
                    string x = conexion.setOpcionModulo(opcion, System.Data.Entity.EntityState.Modified);
                    ShowMessage(x, MessageType.Correcto);
                }
                else
                {
                    string x = conexion.setOpcionModulo(opcion, System.Data.Entity.EntityState.Added);
                    ShowMessage(x, MessageType.Correcto);
                }
                dt = new DataTable();
                llenarGrid();
                DataBind();
                panelGrid.Update();
                limpiarCampos();
            }

        }
        public void limpiarCampos()
        {
            txtCodigo.Text = String.Empty;
            txtNombre.Text = String.Empty;
            txtDescripcion.Text = String.Empty;
            txtEnlace.Text = String.Empty;
            hdnIndex.Text = String.Empty;
            cmbModulo.SelectedIndex = 0;
            btnUpdate.Text = "Guardar";
            requirement8.Visible = false;
        }
    }
}