﻿using SecretariaConnection;
using SecretariaConnection.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ControlSecretaria.Componentes.Seguridad
{
    public partial class FormPermisos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static List<TreeViewModel> listaModulos()
        {
            Repositorio repo = new Repositorio();
            var ds = repo.getTree();
            return ds;
        }

        [WebMethod]
        public static string setPermisos(string permisos)
        {
            return permisos;
        }
    }
}