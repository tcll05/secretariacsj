﻿using SecretariaConnection;
using SecretariaConnection.EFModel;
using SecretariaConnection.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ControlSecretaria.Componentes.Catalogos
{
    public partial class FormJuzgado : System.Web.UI.Page
    {
        Repositorio conexion = new Repositorio();
        private DataTable dt;
        public enum MessageType { Correcto, Error, Informacion, Advertencia };

        protected void ShowMessage(string Message, object type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "toastrs('" + Message + "','" + type + "');", true);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                dt = new DataTable();
                llenarGrid();
            }
        }

        public void llenarGrid()
        {
            try
            {
                dt.Columns.AddRange(new DataColumn[] {
                             new DataColumn("id_juzgado")
                            ,new DataColumn("descripcion")
                            ,new DataColumn("estado")
                        });

                var ds = conexion.getJuzgado();
                string estado = "";
                foreach (JuzgadoViewModel lista in ds)
                {
                    if (lista.estado == false)
                    {
                        estado = "Inactivo";
                    }
                    else estado = "Activo";
                    dt.Rows.Add(lista.id_juzgado, lista.descripcion, estado);
                }
                grdvSalas.DataSource = dt;
                grdvSalas.DataBind();
                grdvSalas.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            catch (Exception ex)
            {
                //ShowMessage(ex.Message, MessageType.Error);
            }
        }

        protected void grdvSalas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dt = new DataTable();
            llenarGrid();

            grdvSalas.PageIndex = e.NewPageIndex;
            DataBind();
            panelGrid.Update();
        }

        protected void hdnIndex_TextChanged(object sender, EventArgs e)
        {
            string[] s = hdnIndex.Text.ToString().Split(';');
            if (s[0].Equals("editar"))
                btnEdit_Click(sender, e);
            else
                btnDelete_Click(sender, e);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            string[] s = hdnIndex.Text.Split(';');
            int index = Convert.ToInt32(s[1]);
            GridViewRow row = grdvSalas.Rows[index - 1];
            //row.FindControl("tribunal").Visible = true;
            txtCodigo.Text = (row.FindControl("id_procedencia") as HiddenField).Value;
            txtNombre.Text = (row.FindControl("descripcion") as Label).Text;
            btnUpdate.Text = "Modificar";
        }

        public bool validarBtn(string msg)
        {
            if (msg.Equals("Modificar")) return true; else return false;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            juzgado proc = new juzgado()
            {
                descripcion = txtNombre.Text,
                usuario_registro = "admin",
                estado = true,
                fecha_registro = DateTime.Now
            };
            if (validarBtn(btnUpdate.Text))
            {
                proc.id_juzgado = Convert.ToInt32(txtCodigo.Text);
                string x = conexion.setJuzgado(proc, System.Data.Entity.EntityState.Modified);
                ShowMessage(x, MessageType.Correcto);
            }
            else
            {
                string x = conexion.setJuzgado(proc, System.Data.Entity.EntityState.Added);
                ShowMessage(x, MessageType.Correcto);
            }
            dt = new DataTable();
            llenarGrid();
            DataBind();
            panelGrid.Update();
            limpiarCampos();

        }
        public void limpiarCampos()
        {
            txtCodigo.Text = String.Empty;
            txtNombre.Text = String.Empty;
            hdnIndex.Text = String.Empty;
            btnUpdate.Text = "Guardar";
        }
    }
}