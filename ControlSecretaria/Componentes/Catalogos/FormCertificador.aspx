﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FormCertificador.aspx.cs" Inherits="ControlSecretaria.Componentes.Catalogos.FormCertificador" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../../Controllers/SalasController.js"></script>
    <link href="../../Content/toastr.css" rel="stylesheet"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../Scripts/toastr.js" type="text/javascript"></script>
    <br />
    <h1>Catalogo de Certificadores</h1>
    <br />
    <asp:UpdatePanel runat="server" ID="panelGrid" UpdateMode="Conditional">
        
        <ContentTemplate>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            Crear Nuevo Certificador
                        </div>
                        <div class="card-body">
                            <asp:TextBox ID="txtCodigo" runat="server" Visible="false" AutoPostBack="true" Width="100%" CssClass="form-control my-1 mr-sm-2" placeholder="Codigo"></asp:TextBox>

                            <div class="form-group">
                                <label for="txtNombre" class="my-1 mr-sm-2">Nombre</label>

                                <asp:TextBox ID="txtNombre" runat="server" Width="100%" CssClass="form-control" placeholder="Nombre Completo"></asp:TextBox>
                                <small id="requirement7" class="form-text text-muted">
                                    <asp:RequiredFieldValidator runat="server" ID="rfvNombre2" ControlToValidate="txtNombre" ValidationGroup="SaveValidation" ErrorMessage="Campo Requerido" ForeColor="red"></asp:RequiredFieldValidator>
                                </small>

                            </div>
                            <div class="form-group">
                                <label for="txtPuesto" class="my-1 mr-sm-2">Puesto</label>

                                <asp:TextBox ID="txtPuesto" runat="server" Width="100%" TextMode="multiline" Style="resize: vertical" CssClass="form-control" placeholder="Puesto"></asp:TextBox>
                                <small id="requirement8" class="form-text text-muted">
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtPuesto" ValidationGroup="SaveValidation" ErrorMessage="Campo Requerido" ForeColor="red"></asp:RequiredFieldValidator>
                                </small>
                            </div>
                        </div>
                        <div class="card-footer">
                            <asp:Button runat="server" ID="btnUpdate" ValidationGroup="SaveValidation" Text="Guardar" CssClass="btn btn-primary"
                                UseSubmitBehavior="false" OnClick="btnUpdate_Click" />
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-12">
                    <asp:TextBox ID="hdnIndex" runat="server" hidden OnTextChanged="hdnIndex_TextChanged"></asp:TextBox>
                    <asp:GridView runat="server" ID="grdvSalas" CssClass="table table-responsive-sm table-hover table-sm table-striped" GridLines="None" EmptyDataText="No hay datos para mostrar" ShowHeaderWhenEmpty="true" OnPageIndexChanging="grdvSalas_PageIndexChanging" DataKeyNames="id_certificador" UseAccessibleHeader="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="10">
                        <HeaderStyle BackColor="#337ab7" ForeColor="White" />
                        <PagerStyle CssClass="pagination-ys" />
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="0%">
                                <ItemTemplate>
                                    <asp:HiddenField ID="id_certificador" runat="server" Value='<%# Eval("id_certificador") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="30%" HeaderText="Nombre">
                                <ItemTemplate>
                                    <asp:Label ID="nombre_certificador" runat="server" Text='<%# Eval("nombre_certificador") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="60%" HeaderText="Puesto">
                                <ItemTemplate>
                                    <asp:Label ID="puesto" runat="server" Text='<%# Eval("puesto") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="10%" HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="estado" runat="server" Text='<%# Eval("estado") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <%--Botones de eliminar y editar producto...--%>
                                    <asp:LinkButton ID="btnEdit"
                                        runat="server" ClientIDMode="AutoID"
                                        OnClientClick="setRow(this, 'editar');" Font-Underline="true">
                                                            <span aria-hidden="true">Editar</span>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnDelete"
                                        runat="server" ClientIDMode="AutoID"
                                        OnClientClick="eliminar(this);"
                                        Font-Underline="true">
                                                <span aria-hidden="true">Eliminar</span>
                                    </asp:LinkButton>
                                    <%-- <asp:Button ID="btnEdit" runat="server" CssClass="btn btn-success btn-sm" Text="Editar" CommandName="editar" OnClick="btnEdit_Click" UseSubmitBehavior="false" />
                                                    <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger btn-sm" Text="Eliminar" CommandName="eliminar" OnClientClick="return confirm('¿Eliminar producto?');" OnClick="btnDelete_Click" />--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
