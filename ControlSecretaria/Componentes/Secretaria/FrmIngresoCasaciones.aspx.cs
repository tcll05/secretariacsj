﻿using SecretariaConnection;
using SecretariaConnection.EFModel;
using SecretariaConnection.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ControlSecretaria.Componentes.Secretaria
{
    public partial class FrmIngresoCasaciones : System.Web.UI.Page
    {
        Repositorio conexion = new Repositorio();
        private DataTable dt;
        string mensaje;
        public void showCodigo(string Message, object type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
        }
        public enum MessageType { Correcto, Error, Informacion, Advertencia };

        protected void ShowMessage(string Message, object type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "toastrs('" + Message + "','" + type + "');", true);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                llenarSalas();
                llenarJuzgados();
                cmbCasacion.Items.Insert(0, new ListItem("Seleccione una casacion", "0"));
                //llenarMantenimiento();
                dt = new DataTable();
                llenarTabla();
                //mostrarFechas();
            }

        }
        public void llenarJuzgados()
        {
            try
            {
                //txtId.Text = "0";
                var ds = conexion.getJuzgado();
                cmbJuzgado.DataSource = ds;
                cmbJuzgado.DataValueField = "id_juzgado";
                cmbJuzgado.DataTextField = "descripcion";
                cmbJuzgado.DataBind();
                cmbJuzgado.Items.Insert(0, new ListItem("Seleccione una corte o juzgado", "0"));
                //comboTribunal.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, MessageType.Error);
            }
        }

        public void llenarMantenimiento(int id_casacion, int id_transaccion)
        {
            //var ds = conexion.getTipoFecha(1);
            var ds3 = conexion.getFechas(id_casacion, id_transaccion);
            if (ds3.Count() > 0)
            {
                cmbTipoFecha.DataSource = ds3;
                cmbTipoFecha.DataValueField = "id_tipo_fecha";
                cmbTipoFecha.DataTextField = "descripcion";
                cmbTipoFecha.DataBind();
            }
            cmbTipoFecha.Items.Insert(0, new ListItem("Seleccione una Fecha", "0"));

            var ds2 = conexion.getObservaciones(id_casacion, id_transaccion);
            if (ds2.Count() > 0)
            {
                cmbObservacionCasacion.DataSource = ds2;
                cmbObservacionCasacion.DataValueField = "id_tipo_observacion";
                cmbObservacionCasacion.DataTextField = "descripcion";
                cmbObservacionCasacion.DataBind();
            }
            cmbObservacionCasacion.Items.Insert(0, new ListItem("Seleccione una Observacion", "0"));
        }

        public void llenarSalas()
        {
            try
            {
                //txtId.Text = "0";
                var ds = conexion.getSalas();
                cmbSala.DataSource = ds;
                cmbSala.DataValueField = "id_salas";
                cmbSala.DataTextField = "descripcion";
                cmbSala.DataBind();
                cmbSala.Items.Insert(0, new ListItem("Seleccione una sala", "0"));
                //comboTribunal.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, MessageType.Error);
            }
        }

        public void llenarTabla()
        {
            try
            {
                dt.Columns.AddRange(new DataColumn[] {
                            new DataColumn("id_casacion"),
                             new DataColumn("id_tipo_casacion")
                            ,new DataColumn("id_juzgado")
                            ,new DataColumn("numero_registro")
                            ,new DataColumn("nombre_apoderado")
                            ,new DataColumn("nombre_demandante")
                            ,new DataColumn("nombre_demandado")
                            ,new DataColumn("sala")
                            ,new DataColumn("casacion")
                            ,new DataColumn("procedencia")
                        });

                var ds = conexion.getCasacion();
                string procedencia = "";
                foreach (CasacionViewModel lista in ds)
                {

                    if (lista.procedencia != "")
                    {
                        procedencia = lista.procedencia;
                    }
                    else procedencia = lista.juzgado.descripcion;
                    dt.Rows.Add(lista.id_casacion, lista.id_tipo_casacion, lista.id_juzgado, lista.numero_registro, lista.nombre_apoderado,
                        lista.nombre_demandante, lista.nombre_demandado, lista.tipoCasacion.salas.descripcion, lista.tipoCasacion.descripcion, procedencia);
                }
                grdvSalas.DataSource = dt;
                grdvSalas.DataBind();
                grdvSalas.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, MessageType.Error);
            }
        }

        public void llenarCasaciones(int id)
        {
            try
            {
                //txtId.Text = "0";
                cmbCasacion.Enabled = true;
                var ds = conexion.getTipoCasacion(id);
                cmbCasacion.DataSource = ds;
                cmbCasacion.DataValueField = "id_tipo_casacion";
                cmbCasacion.DataTextField = "descripcion";
                cmbCasacion.DataBind();
                cmbCasacion.Items.Insert(0, new ListItem("Seleccione una casacion", "0"));
                //comboTribunal.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, MessageType.Error);
            }
        }
        public void validarSalas()
        {
            cmbJuzgado.Visible = true;
            txtProcedencia.Visible = false;
            lblCorte.InnerText = "Corte o Juzgado";
            panelMotivo.Visible = false;
            lblDemandante.InnerText = "En la demanda promovida por";
            lblMotivo.InnerText = "Motivo de la demanda";
            if (cmbSala.SelectedItem.Text.Contains("PENAL"))
            {
                lblDemandante.InnerText = "En la causa instruida contra";
                lblDemandado.InnerText = "En perjuicio de";
                lblMotivo.InnerText = "Por el delito de";
                panelMotivo.Visible = true;
            }
            if (cmbSala.SelectedItem.Text.Contains("LABORAL") || cmbSala.SelectedItem.Text.Contains("CIVIL"))
            {
                lblDemandado.InnerText = "En contra de";
            }


        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int corte = 0;
            if (lblCorte.InnerHtml.Contains("procedencia")) corte = 1;
            else corte = Convert.ToInt32(cmbJuzgado.SelectedValue);
            string desc = getDescripcion();
            int id_sala = Convert.ToInt32(cmbSala.SelectedValue);
            DateTime fecha = DateTime.Now;
            int correlativo = conexion.correlativoCasaciones(id_sala, fecha.Year.ToString()) + 1;
            string codigo = asignarCodigo(id_sala);
            try
            {
                casacion casacion = new casacion()
                {
                    consta_de = txtConsta.Text,
                    fecha_registro = DateTime.Now,
                    id_juzgado = corte,
                    id_tipo_casacion = Convert.ToInt32(cmbCasacion.SelectedValue),
                    motivo_demanda = txtMotivoDemanda.Text,
                    nombre_apoderado = txtApoderado.Text,
                    nombre_demandado = txtDemandado.Text,
                    nombre_demandante = txtDemandante.Text,
                    estado = true,
                    descripcion = desc,
                    id_letrado = 1,
                    procedencia = txtProcedencia.Text,
                    usuario_registro = "admin",
                    completo = false,
                };
                if (btnUpdate.Text.Equals("Guardar"))
                {

                    casacion.numero_correlativo = correlativo;
                    casacion.numero_registro = codigo;
                    int new_id = conexion.setCasacion(casacion, System.Data.Entity.EntityState.Added);
                    fecha_casacion fecha_casacion = new fecha_casacion()
                    {
                        descripcion = txtFechaConsta.Text,
                        estado = true,
                        fecha_registro = fecha,
                        id_tipo_fecha = 13, //fecha consta de
                        usuario_registro = "admin",
                        id_casacion = new_id
                    };
                    conexion.setFechaCasacion(fecha_casacion, System.Data.Entity.EntityState.Added);
                    ShowMessage("Registro guardado exitosamente", MessageType.Correcto);
                    showCodigo("El codigo de casacion es: <strong>" + codigo + "</strong>", MessageType.Informacion);
                }
                else
                {
                    var tmp = conexion.buscarCasacion(Convert.ToInt32(txtCodigo.Text));
                    casacion.id_casacion = Convert.ToInt32(txtCodigo.Text);
                    casacion.numero_registro = tmp.numero_registro;
                    casacion.numero_correlativo = tmp.numero_correlativo;
                    conexion.setCasacion(casacion, System.Data.Entity.EntityState.Modified);
                    ShowMessage("Registro modificado exitosamente", MessageType.Correcto);
                    showCodigo("El codigo de casacion es: <strong>" + casacion.numero_registro + "</strong>", MessageType.Informacion);
                }
                limpiarCampos();
            }
            catch (Exception ex)
            {
                ShowMessage("error " + ex.Message, MessageType.Error);
            }
        }

        public void limpiarCampos()
        {
            nav_mantenimiento.Visible = false;
            mantenimiento.Visible = false;
            btnUpdate.Visible = true;
            cmbSala.Enabled = true;
            cmbCasacion.Enabled = true; ;
            txtApoderado.Text = string.Empty;
            txtCodigo.Text = string.Empty;
            txtConsta.Text = string.Empty;
            txtDemandado.Text = string.Empty;
            txtDemandante.Text = string.Empty;
            txtFechaConsta.Text = string.Empty;
            txtMotivoDemanda.Text = string.Empty;
            txtProcedencia.Text = string.Empty;
            hdnIndex.Text = "0";
            cmbJuzgado.SelectedIndex = 0;
            cmbSala.SelectedIndex = 0;
            cmbCasacion.SelectedIndex = 0;
            btnUpdate.Text = "Guardar";
            txtObservacion.Text = String.Empty;
            txtResolucion.Text = String.Empty;
            chkCompleto.Checked = false;
            //cmbObservacionCasacion.SelectedIndex = 0;
            txtFechaTipo.Text = String.Empty;
            //cmbTipoFecha.SelectedIndex = 0;
            validarSalas();
        }

        private string getDescripcion()
        {
            string descripcion = "";
            if (txtApoderado.Text != "")
                descripcion += lblPresentado.InnerHtml + ": " + txtApoderado.Text + " ";
            if (txtDemandante.Text != "")
                descripcion += lblDemandante.InnerHtml + ": " + txtDemandante.Text + " ";
            if (txtMotivoDemanda.Text != "")
                descripcion += lblMotivo.InnerHtml + ": " + txtMotivoDemanda.Text + " ";
            if (txtDemandado.Text != "")
                descripcion += lblDemandado.InnerHtml + ": " + txtDemandado.Text + " ";
            if (cmbJuzgado.SelectedIndex != 0)
                descripcion += lblCorte.InnerHtml + ": " + cmbJuzgado.SelectedItem.Text + " ";
            else
                descripcion += lblCorte.InnerHtml + ": " + txtProcedencia.Text + " ";
            if (txtConsta.Text != "")
                descripcion += "Consta de: " + txtConsta.Text + " ";
            return descripcion;
        }


        protected void cmbSala_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbSala.SelectedIndex != 0)
            {
                llenarCasaciones(Convert.ToInt32(cmbSala.SelectedValue));
                validarSalas();
                string pruebas = asignarCodigo(Convert.ToInt32(cmbSala.SelectedValue));
            }
        }

        public string asignarCodigo(int id_sala)
        {
            int correlativo = 0; string codigo = "";
            if (cmbSala.SelectedIndex != 0)
            {
                DateTime fecha = DateTime.Now;
                correlativo = conexion.correlativoCasaciones(id_sala, fecha.Year.ToString()) + 1;
                var sala = conexion.buscarSala(id_sala);

                codigo = sala.alias + "-" + correlativo + "-" + fecha.Year.ToString().Substring(2);
                return codigo;
            }
            return "";

        }

        protected void cmbCasacion_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbJuzgado.Visible = true;
            txtProcedencia.Visible = false;
            lblCorte.InnerText = "Corte o Juzgado";
            lblDemandante.InnerText = "En la demanda promovida por";
            lblPresentado.InnerText = "Presentado por";
            lblMotivo.InnerText = "Motivo de la demanda";
            panelMotivo.Visible = false;

            if (cmbCasacion.SelectedItem.Text.Contains("CASACION LABORAL") || cmbCasacion.SelectedItem.Text.Contains("ADEFECTUM VIDENDI") || cmbCasacion.SelectedItem.Text.Contains("CASACION PENAL"))
            {
                panelMotivo.Visible = true;
            }
            if (cmbSala.SelectedItem.Text.Contains("PENAL"))
            {
                lblDemandante.InnerText = "En la causa instruida contra";
                lblDemandado.InnerText = "En perjuicio de";
                //lblMotivo.InnerText = "Por el delito de";
                //panelMotivo.Visible = true;
                if (cmbCasacion.SelectedItem.Text.Contains("SUPLICATORIO") || cmbCasacion.SelectedItem.Text.Contains("APELACION"))
                    panelMotivo.Visible = true;
            }

            if (cmbSala.SelectedItem.Text.Contains("LABORAL") || cmbSala.SelectedItem.Text.Contains("PENAL"))
            {
                if (cmbCasacion.SelectedItem.Text.Contains("SUPLICATORIO") || cmbCasacion.SelectedItem.Text.Contains("APELACION"))
                {
                    lblPresentado.InnerText = "Procedente de";
                }
            }
            if (cmbCasacion.SelectedItem.Text.Contains("ASISTENCIA LEGAL"))
            {
                cmbJuzgado.Visible = false;
                lblCorte.InnerHtml = "Con procedencia de";
                txtProcedencia.Visible = true;
            }
        }

        protected void hdnIndex_TextChanged(object sender, EventArgs e)
        {
            string[] s = hdnIndex.Text.ToString().Split(';');
            if (s[0].Equals("editar"))
                btnEdit_Click(sender, e);
            else
            {
                //limpiarCampos();

                btnEdit_Click(sender, e);
                procesar();
            }
        }
        public void procesar()
        {
            string[] s = hdnIndex.Text.Split(';');
            int index = Convert.ToInt32(s[1]);
            //limpiarCampos();
            GridViewRow row = grdvSalas.Rows[index - 1];
            int id = Convert.ToInt32((row.FindControl("id_casacion") as HiddenField).Value);
            llenarMantenimiento(id, 1);
            txtCodigo.Text = Convert.ToString(id);
            mantenimiento.Visible = true;
            nav_mantenimiento.Visible = true;
            btnUpdate.Visible = false;
            mostrarFechas(id);
            mostrarObservaciones(id);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {

            cmbSala.Enabled = false;
            cmbCasacion.Enabled = false;
            cmbJuzgado.SelectedIndex = 0;
            string[] s = hdnIndex.Text.Split(';');
            int index = Convert.ToInt32(s[1]);
            //limpiarCampos();
            GridViewRow row = grdvSalas.Rows[index - 1];
            //txtCodigo.Text = (row.FindControl("id_tipo_observacion") as HiddenField).Value;
            //txtNombre.Text = (row.FindControl("descripcion") as Label).Text;
            int id = Convert.ToInt32((row.FindControl("id_casacion") as HiddenField).Value);
            var fecha = conexion.buscarFechaCasacion(id, 13);
            var casacion = conexion.buscarCasacion(id);
            txtApoderado.Text = (row.FindControl("nombre_apoderado") as Label).Text;
            txtCodigo.Text = Convert.ToString(id);
            txtConsta.Text = casacion.consta_de;
            txtDemandado.Text = (row.FindControl("nombre_demandado") as Label).Text;
            txtDemandante.Text = (row.FindControl("nombre_demandante") as Label).Text;
            //txtFechaConsta.Text = casacion.fecha;
            if (!String.IsNullOrEmpty(casacion.motivo_demanda))
            {
                txtMotivoDemanda.Text = casacion.motivo_demanda;
            }
            if (!String.IsNullOrEmpty(casacion.procedencia))
                txtProcedencia.Text = casacion.procedencia;
            else cmbJuzgado.SelectedValue = Convert.ToString(casacion.id_juzgado);
            txtFechaConsta.Text = fecha.descripcion;
            cmbSala.SelectedValue = Convert.ToString(casacion.tipoCasacion.id_salas);
            llenarCasaciones(Convert.ToInt32(cmbSala.SelectedValue));
            cmbCasacion.SelectedValue = Convert.ToString(casacion.id_tipo_casacion);
            if (!String.IsNullOrEmpty(casacion.resolucion))
                txtResolucion.Text = casacion.resolucion;
            chkCompleto.Checked = casacion.completo;

            validarSalas();
            cmbCasacion_SelectedIndexChanged(sender, e);
            btnUpdate.Text = "Modificar";
        }

        protected void grdvSalas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dt = new DataTable();
            llenarTabla();

            grdvSalas.PageIndex = e.NewPageIndex;
            DataBind();
            panel.Update();
        }
        public string getMensaje(string tipo, string fecha)
        {
            int idtmp = 0;
            //string boton = "<button value=\"" + idtmp + "\" class=\"btn btn-primary pull-right fas fa-pencil\">" +
                                    //"</button>";
            string mensaje = "<strong>" + tipo + "</strong><br/><p>" + fecha +  "</p><hr/>";
            return mensaje;
        }

        protected void btnAddFecha_Click(object sender, EventArgs e)
        {

            fecha_casacion fecha = new fecha_casacion()
            {
                id_casacion = Convert.ToInt32(txtCodigo.Text),
                descripcion = txtFechaTipo.Text,
                id_tipo_fecha = Convert.ToInt32(cmbTipoFecha.SelectedValue),
                estado = true,
                fecha_registro = DateTime.Now,
                usuario_registro = "admin"
            };

            conexion.setFechaCasacion(fecha, System.Data.Entity.EntityState.Added);
            mostrarFechas(Convert.ToInt32(txtCodigo.Text));
            mostrarObservaciones(Convert.ToInt32(txtCodigo.Text));
            txtFechaTipo.Text = String.Empty;
            cmbTipoFecha.Items.Clear();
            cmbObservacionCasacion.Items.Clear();
            llenarMantenimiento(Convert.ToInt32(txtCodigo.Text), 1);
            cmbTipoFecha.SelectedIndex = 0;
        }
        public void mostrarObservaciones(int id)
        {
            string mensaje = getObservaciones(id);
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "setObservacion('" + mensaje + "');", true);
        }
        public void mostrarFechas(int id)
        {
            string mensaje = getFechas(id);
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "setFecha('" + mensaje + "');", true);
        }

        public string getFechas(int id)
        {
            string mensaje = "";
            var ds = conexion.getFechas(id);
            foreach (FechaCasacionViewModel fecha in ds)
            {
                mensaje += getMensaje(fecha.tipoFecha.descripcion, fecha.descripcion);
            }

            return mensaje;
        }
        public string getObservaciones(int id)
        {
            string mensaje = "";
            var ds = conexion.getObservaciones(id);
            foreach (ObservacionCasacionViewModel obs in ds)
            {
                mensaje += getMensaje(obs.tipoObservacion.descripcion, obs.descripcion);
            }

            return mensaje;
        }

        protected void btnAddObservacion_Click(object sender, EventArgs e)
        {
            observacion_casacion observacion = new observacion_casacion()
            {
                id_casacion = Convert.ToInt32(txtCodigo.Text),
                descripcion = txtObservacion.Text,
                id_tipo_observacion = Convert.ToInt32(cmbObservacionCasacion.SelectedValue),
                estado = true,
                fecha_registro = DateTime.Now,
                usuario_registro = "admin"
            };
            conexion.setObservacionCasacion(observacion, System.Data.Entity.EntityState.Added);
            txtObservacion.Text = String.Empty;
            cmbObservacionCasacion.SelectedIndex = 0;
            cmbTipoFecha.Items.Clear();
            cmbObservacionCasacion.Items.Clear();
            llenarMantenimiento(Convert.ToInt32(txtCodigo.Text), 1);
            mostrarObservaciones(Convert.ToInt32(txtCodigo.Text));
            mostrarFechas(Convert.ToInt32(txtCodigo.Text));
        }

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            cmbTipoFecha.Items.Clear();
            cmbObservacionCasacion.Items.Clear();
            limpiarCampos();
            nav_mantenimiento.Visible = false;
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            int corte = 0;
            if (lblCorte.InnerHtml.Contains("procedencia")) corte = 1;
            else corte = Convert.ToInt32(cmbJuzgado.SelectedValue);
            string desc = getDescripcion();
            int id_sala = Convert.ToInt32(cmbSala.SelectedValue);
            DateTime fecha = DateTime.Now;
            int correlativo = conexion.correlativoCasaciones(id_sala, fecha.Year.ToString()) + 1;
            string codigo = asignarCodigo(id_sala);
            try
            {
                casacion casacion = new casacion()
                {
                    consta_de = txtConsta.Text,
                    fecha_registro = DateTime.Now,
                    id_juzgado = corte,
                    id_tipo_casacion = Convert.ToInt32(cmbCasacion.SelectedValue),
                    motivo_demanda = txtMotivoDemanda.Text,
                    nombre_apoderado = txtApoderado.Text,
                    nombre_demandado = txtDemandado.Text,
                    nombre_demandante = txtDemandante.Text,
                    estado = true,
                    resolucion = txtResolucion.Text,
                    descripcion = desc,
                    id_letrado = 1,
                    procedencia = txtProcedencia.Text,
                    usuario_registro = "admin",
                    completo = chkCompleto.Checked,
                };
                var tmp = conexion.buscarCasacion(Convert.ToInt32(txtCodigo.Text));
                casacion.id_casacion = Convert.ToInt32(txtCodigo.Text);
                casacion.numero_registro = tmp.numero_registro;
                casacion.numero_correlativo = tmp.numero_correlativo;
                conexion.setCasacion(casacion, System.Data.Entity.EntityState.Modified);
                ShowMessage("Registro modificado exitosamente", MessageType.Correcto);
                showCodigo("El codigo de casacion es: <strong>" + casacion.numero_registro + "</strong>", MessageType.Informacion);
                limpiarCampos();
            }
            catch (Exception ex)
            {

            }

        }
    }
}