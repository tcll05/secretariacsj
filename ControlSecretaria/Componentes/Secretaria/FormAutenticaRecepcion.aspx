﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FormAutenticaRecepcion.aspx.cs" Inherits="ControlSecretaria.Componentes.Secretaria.FormAutenticaRecepcion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../../Controllers/SalasController.js"></script>
    <link href="../../Content/toastr.css" rel="stylesheet"/>
    <script src="../../Scripts/all.js"></script>
    <script src="../../Scripts/vue.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../Scripts/toastr.js" type="text/javascript"></script>
    <br />
    <h1>Recepcion de Autenticas</h1>
    <br />
    <asp:UpdatePanel runat="server" ID="panelGrid" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row">
                <div class="col-md-12">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-recepcion" role="tab" aria-controls="nav-recepcion" aria-selected="true">Recepcion de Autenticas</a>
                            <a class="nav-item nav-link" id="nav_registro" runat="server" visible="false" data-toggle="tab" href="#nav-registro" role="tab" aria-controls="nav-registro" aria-selected="false">Registro</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-recepcion" role="tabpanel" aria-labelledby="nav-home-tab">
                            <%-- recepcion --%>
                            <div class="card">
                                <div class="card-body">
                                    <asp:TextBox ID="txtCodigo" runat="server" Visible="false" AutoPostBack="true" Width="100%" CssClass="form-control my-1 mr-sm-2" placeholder="Codigo"></asp:TextBox>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <label for="txtSolicitante" class="my-1 mr-sm-2">Solicitante</label>
                                            <asp:TextBox ID="txtSolicitante" runat="server" Width="100%" CssClass="form-control" placeholder="Nombre del Solicitante"></asp:TextBox>
                                            <small id="requirement7" visible="false" class="form-text text-muted">
                                                <asp:RequiredFieldValidator runat="server" ID="rfvNombre2" ControlToValidate="txtSolicitante" ValidationGroup="SaveValidation" ErrorMessage="Campo Requerido" ForeColor="red"></asp:RequiredFieldValidator>
                                            </small>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="txtFirma" class="my-1 mr-sm-2">Firma a Autenticar</label>
                                            <asp:TextBox ID="txtFirma" runat="server" Width="100%" CssClass="form-control" placeholder="Nombre de la firma a autenticar"></asp:TextBox>
                                            <small id="small1" visible="false" class="form-text text-muted">
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtFirma" ValidationGroup="SaveValidation" ErrorMessage="Campo Requerido" ForeColor="red"></asp:RequiredFieldValidator>
                                            </small>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <label for="txtRecibo" class="my-1 mr-sm-2">Numero de Recibo</label>
                                            <asp:TextBox ID="txtRecibo" runat="server" Width="100%" CssClass="form-control" placeholder="Numero de recibo"></asp:TextBox>
                                            <small id="small2" visible="false" class="form-text text-muted">
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtFirma" ValidationGroup="SaveValidation" ErrorMessage="Campo Requerido" ForeColor="red"></asp:RequiredFieldValidator>
                                            </small>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="cmbSolicitud" class="my-1 mr-sm-2">Tipo de Solicitud</label>
                                            <asp:DropDownList ID="cmbSolicitud" runat="server" CssClass="form-control" placeholder="Tipo de Solicitud"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <hr />
                                    <div id="panelFechas" runat="server" visible="false">
                                        <div class="form-row">
                                            <div class="col-sm-6">
                                                <div class="form-row" style="text-align: center;">
                                                    <div class="col-md-5">
                                                        <asp:DropDownList runat="server" ID="cmbTipoFecha" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <asp:TextBox ID="txtFechaTipo" CssClass="form-control" placeholder="Seleccione la Fecha" runat="server"></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MM/yyyy" runat="server" BehaviorID="txtFechaTipo_CalendarExtender" TargetControlID="txtFechaTipo" />

                                                    </div>
                                                    <div class="col-md-1" id="btn1">
                                                        <asp:LinkButton ID="btnAddFecha"
                                                            runat="server"
                                                            CssClass="btn btn-primary mb-2" OnClick="btnAddFecha_Click">
                                                            <span aria-hidden="true" class="fas fa-check"></span>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                                <br />
                                                <div class="form-row">
                                                    <div class="col-md-11">
                                                        <div id="listaFechas">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-row" style="text-align: right;">
                                                    <div class="col-md-1"></div>
                                                    <div class="col-md-5">
                                                        <asp:DropDownList runat="server" ID="cmbObservacionCasacion" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <asp:TextBox ID="txtObservacion" CssClass="form-control" placeholder="Observacion" TextMode="MultiLine" Style="resize: vertical;" runat="server"></asp:TextBox>

                                                    </div>
                                                    <div class="col-md-1">
                                                        <asp:LinkButton ID="btnAddObservacion"
                                                            runat="server" OnClick="btnAddObservacion_Click"
                                                            CssClass="btn btn-primary mb-2">
                                                            <span aria-hidden="true" class="fas fa-check"></span>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <asp:Button runat="server" ID="btnUpdate" ValidationGroup="SaveValidation" Text="Guardar" CssClass="btn btn-primary"
                                        UseSubmitBehavior="false" OnClick="btnUpdate_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-registro" role="tabpanel" aria-labelledby="nav_registro">
                            <%-- mantenimiento --%>
                            <div id="mantenimiento" runat="server" visible="true" class="card">
                                <div style="align-self: center; width: 50%;">
                                    <div class="card-body" style="text-align: justify;">
                                        <div class="justify-content-center">
                                            <div class="alert alert-info" role="alert">
                                                <strong id="autenticaLbl" runat="server"></strong>
                                            </div>

                                            <div class="form-row">
                                                <p>
                                                    La Infrascrita, Secretaria General de la Corte Suprema de Justicia CERTIFICA:
                                                    <br />
                                                    Que es auténtica la firma que antecede y dice:
                                                <br /><br />
                                                </p>
                                                <div id="buscarAutenticador">
                                                    <div class="form-row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <asp:TextBox ID="txtBuscarAutenticador" v-model="search" runat="server" CssClass="form-control" placeholder="Buscar por nombre" autocomplete="off"></asp:TextBox>
                                                            </div>
                                                            <%-- <asp:Button type="button" ID="btnBuscar" runat="server" CssClass="btn btn-info mb-2" Text="Buscar" CommandName="Buscar" OnClick="btnBuscar_Click" />--%>
                                                        </div>
                                                    </div>
                                                    <hr />
                                                    <div class="form-row">
                                                        <div class="col-md-12">
                                                            <ul class="list-group" id="lista1">
                                                                <asp:ListBox runat="server" ID="listAutenticador" CssClass="form-control">
                                                                    <asp:ListItem onClick="setAutenticador()" v-for="item in filteredList" v-bind:value="item.id_certificador">{{item.nombre_certificador}}</asp:ListItem>
                                                                </asp:ListBox>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <p>
                                                        puesta en su caracter de:
                                                    <br />
                                                        <strong id="puesto" runat="server"></strong>
                                                        <br />
                                                    </p>
                                                </div>
                                                <%--<asp:DropDownList ID="cmbAutenticador" runat="server" onchange="setAutenticador()" CssClass="form-control" placeholder="Autenticador"></asp:DropDownList>
                                                <asp:DropDownList ID="cmbPuestoAutenticador" runat="server" CssClass="form-control" placeholder="Autenticador"></asp:DropDownList>--%>
                                                <br />
                                                <p>
                                                    En el documento consistente en:
                                                </p>
                                                <br />
                                                <asp:TextBox ID="txtConsiste" runat="server" TextMode="MultiLine" Rows="2" Style="resize: vertical;" Width="100%" CssClass="form-control"></asp:TextBox>
                                                <br />
                                                <p id="lblLugar" runat="server"></p>
                                                <br />
                                            </div>
                                            <br />
                                            <div class="form-row">
                                                <div class="form-inline">
                                                    <label for="cmbSecretario">Secretaria(o): &nbsp</label>
                                                    <asp:DropDownList ID="cmbSecretario" onchange="setSecretario()" runat="server" CssClass="form-control" placeholder="Secretario(a)"></asp:DropDownList>
                                                    <asp:DropDownList ID="cmbPuestoSecretario" runat="server" CssClass="form-control" placeholder="Autenticador"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <strong><center id="puestoSecretario" runat="server"></center></strong>
                                            <hr />
                                            <div class="form-row">
                                            </div>
                                        </div>
                                        
                                        <%--</div>--%>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <asp:Button runat="server" ID="btnGuardar" Text="Guardar" CssClass="btn btn-primary"
                                        UseSubmitBehavior="false" OnClick="btnGuardar_Click" />

                                    <asp:Button runat="server" ID="btnRegresar" Text="Regresar" CssClass="btn btn-info"
                                        UseSubmitBehavior="false" OnClick="btnRegresar_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    
            <br />
            </div>
            <br />
            <div class="row">
                <div class="col-md-12">
                    
                   <asp:TextBox ID="hdnIndex" runat="server" OnTextChanged="hdnIndex_TextChanged"></asp:TextBox>
                    <asp:GridView runat="server" ID="grdvSalas" CssClass="table table-responsive-sm table-hover table-sm table-striped" GridLines="None" EmptyDataText="No hay datos para mostrar" ShowHeaderWhenEmpty="true" OnPageIndexChanging="grdvSalas_PageIndexChanging" DataKeyNames="id_autentica" UseAccessibleHeader="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="10">
                        <HeaderStyle BackColor="#337ab7" ForeColor="White" />
                        <PagerStyle CssClass="pagination-ys" />
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="0%">
                                <ItemTemplate>
                                    <asp:HiddenField ID="id_autentica" runat="server" Value='<%# Eval("id_autentica") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="0%">
                                <ItemTemplate>
                                    <asp:HiddenField ID="id_tipo_solicitud" runat="server" Value='<%# Eval("id_tipo_solicitud") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="10%" HeaderText="Numero">
                                <ItemTemplate>
                                    <asp:Label ID="numero_autentica" runat="server" Text='<%# Eval("numero_autentica") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="30%" HeaderText="Nombre Apoderado">
                                <ItemTemplate>
                                    <asp:Label ID="nombre_apoderado" runat="server" Text='<%# Eval("nombre_apoderado") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="30%" HeaderText="Nombre Autenticador">
                                <ItemTemplate>
                                    <asp:Label ID="nombre_autenticador" runat="server" Text='<%# Eval("nombre_autenticador") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="30%" HeaderText="Tipo de solicitud">
                                <ItemTemplate>
                                    <asp:Label ID="tipo_solicitud" runat="server" Text='<%# Eval("tipo_solicitud") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <%--Botones de eliminar y editar producto...--%>
                                    <asp:LinkButton ID="btnEdit"
                                        runat="server" ClientIDMode="AutoID"
                                        OnClientClick="setRow(this, 'editar');" Font-Underline="true">
                                                            <span aria-hidden="true">Editar</span>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnDelete"
                                        runat="server" ClientIDMode="AutoID"
                                        OnClientClick="setRow(this, 'procesar');"
                                        Font-Underline="true">
                                                <span aria-hidden="true">Procesar</span>
                                    </asp:LinkButton>
                                    <%-- <asp:Button ID="btnEdit" runat="server" CssClass="btn btn-success btn-sm" Text="Editar" CommandName="editar" OnClick="btnEdit_Click" UseSubmitBehavior="false" />
                                                    <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger btn-sm" Text="Eliminar" CommandName="eliminar" OnClientClick="return confirm('¿Eliminar producto?');" OnClick="btnDelete_Click" />--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                     <asp:HiddenField ID="TabName" runat="server" />
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <%--<asp:AsyncPostBackTrigger ControlID="hdnAutenticador" EventName="TextChanged" />--%>
        </Triggers>
    </asp:UpdatePanel>
    <script>
        function ShowMessage(message, messagetype) {
            var cssclass;
            switch (messagetype) {
                case 'Correcto':
                    messagetype = '';
                    cssclass = 'alert-success'
                    break;
                case 'Error':
                    cssclass = 'alert-danger'
                    break;
                case 'Advertencia':
                    cssclass = 'alert-warning'
                    break;
                default:
                    cssclass = 'alert-info'
            }
            $('.modal-backdrop').remove()
            $(document.body).removeClass("modal-open");
            $('#alert_container').append('<div id="alert_div" style="margin: 0 0.5%; -webkit-box-shadow: 3px 4px 6px #999; opacity:100;" class="alert fade in ' + cssclass + '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>' + messagetype + '!</strong> <span>' + message + '</span></div>');
        }

        function setFecha(mensaje) {
            $('#listaFechas').empty();
            $('#listaFechas').append(mensaje);

        }
        function setObservacion(mensaje) {
            $('#listaObservaciones').empty();
            $('#listaObservaciones').append(mensaje);
        }

        function setSecretario() {
            var id = $("#<%=cmbSecretario.ClientID %>").val();//prop('selectedIndex');
            $("#<%=cmbPuestoSecretario.ClientID %>").val(id);// + "";
        }
        function getCertificador() {
            $.ajax({
                type: "GET",
                url: "FormAutenticaRecepcion.aspx/listaCertificador",
                data: '',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) { sortData2(data.d); },
                error: function (data) { console.log(data) }
            });
            
            //$.getJSON("FormAutenticaRecepcion/listaCertificador",
            //    function (Data) {
            //        console.log("llego2");
            //        sortData(Data)
            //    });
        }
        function sortData2(datos) {
            console.log(datos)
            console.log(datos[0].nombre_certificador)
            var example1 = new Vue({
                el: '#buscarAutenticador',
                data: {
                    search: '',
                    datos: datos,
                    puesto: ''
                },
                computed: {
                    filteredList() {
                        return this.datos.filter(post => {
                            return post.nombre_certificador.toLowerCase().includes(this.search.toLowerCase())
                        })
                    }
                }  
            })
        }

        function setAutenticador() {
            var list = document.getElementById("<%=listAutenticador.ClientID %>");
            var selectedText = list.options[list.selectedIndex].innerHTML;
            var selectedValue = list.value;

            console.log(selectedText + ", " + selectedValue);
            
        }
    </script>
</asp:Content>
