﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FrmIngresoCasaciones.aspx.cs" Inherits="ControlSecretaria.Componentes.Secretaria.FrmIngresoCasaciones" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../../Controllers/SalasController.js"></script>
    <link href="../../Content/toastr.css" rel="stylesheet"/>
    <script src="../../Scripts/all.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../Scripts/toastr.js" type="text/javascript"></script>
    <br />
    <h1>Registro de Casaciones</h1>
    <br />
    <div class="messagealert" id="alert_container">
    </div>
    <br />
    <asp:UpdatePanel runat="server" ID="panel" UpdateMode="Conditional">
        <ContentTemplate>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-registro" role="tab" aria-controls="nav-registro" aria-selected="true">Registro de Casaciones</a>
                    <a class="nav-item nav-link" id="nav_mantenimiento" runat="server" visible="false" data-toggle="tab" href="#nav-mantenimiento" role="tab" aria-controls="nav-mantenimiento" aria-selected="false">Mantenimiento</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-registro" role="tabpanel" aria-labelledby="nav-home-tab">
                    <%-- registro --%>
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row" style="padding-bottom: 15px;">
                                <div class="col-md-6">
                                    <asp:TextBox ID="txtCodigo" runat="server" Visible="false" AutoPostBack="true" Width="100%" CssClass="form-control my-1 mr-sm-2" placeholder="Codigo"></asp:TextBox>
                                    <div class="form-group">
                                        <label for="cmbSala" class="my-1 mr-sm-2">Sala</label>
                                        <asp:DropDownList ID="cmbSala" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="cmbSala_SelectedIndexChanged" placeholder="Sala"></asp:DropDownList>
                                        <small id="requirement8" runat="server" visible="false" class="form-text text-muted">
                                            <label id="RequiredFieldValidator1">Campo Requerido</label>
                                        </small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cmbCasacion" class="my-1 mr-sm-2">Casacion</label>
                                        <asp:DropDownList Enabled="false" ID="cmbCasacion" OnSelectedIndexChanged="cmbCasacion_SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control" placeholder="Casacion"></asp:DropDownList>
                                        <small id="Small1" runat="server" visible="false" class="form-text text-muted">
                                            <label id="RequiredFieldValidator2">Campo Requerido</label>
                                        </small>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="lblPresentado" runat="server" for="txtApoderado" class="my-1 mr-sm-2">Presentado por</label>

                                        <asp:TextBox ID="txtApoderado" runat="server" Width="100%" CssClass="form-control" placeholder="Nombre apoderado legal"></asp:TextBox>
                                        <small id="requirement7" visible="false" class="form-text text-muted">
                                            <asp:RequiredFieldValidator runat="server" ID="rfvNombre2" ControlToValidate="txtApoderado" ValidationGroup="SaveValidation" ErrorMessage="Campo Requerido" ForeColor="red"></asp:RequiredFieldValidator>
                                        </small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="lblCorte" runat="server" for="cmbJuzgado" class="my-1 mr-sm-2">Corte o Juzgado</label>
                                        <asp:TextBox ID="txtProcedencia" runat="server" Visible="false" placeholder="Procedencia" CssClass="form-control"></asp:TextBox>
                                        <asp:DropDownList ID="cmbJuzgado" runat="server" CssClass="form-control" placeholder="Corte o Juzgado"></asp:DropDownList>
                                        <small id="Small2" runat="server" visible="false" class="form-text text-muted">
                                            <label id="RequiredFieldValidator3">Campo Requerido</label>
                                        </small>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="lblDemandante" runat="server" for="txtDemandante" class="my-1 mr-sm-2">En la demanda promovida por</label>

                                        <asp:TextBox ID="txtDemandante" runat="server" Width="100%" CssClass="form-control" placeholder="Nombre del demandante"></asp:TextBox>
                                        <small id="small3" visible="false" class="form-text text-muted">
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtDemandante" ValidationGroup="SaveValidation" ErrorMessage="Campo Requerido" ForeColor="red"></asp:RequiredFieldValidator>
                                        </small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="lblDemandado" runat="server" for="txtDemandado" class="my-1 mr-sm-2">En contra de</label>

                                        <asp:TextBox ID="txtDemandado" runat="server" Width="100%" CssClass="form-control" placeholder="Nombre del demandado"></asp:TextBox>
                                        <small id="small4" visible="false" class="form-text text-muted">
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="txtDemandado" ValidationGroup="SaveValidation" ErrorMessage="Campo Requerido" ForeColor="red"></asp:RequiredFieldValidator>
                                        </small>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-12">
                                    <div class="form-group" id="panelMotivo" runat="server" visible="false">
                                        <label id="lblMotivo" runat="server" for="txtMotivoDemanda" class="my-1 mr-sm-2">Por el delito de</label>

                                        <asp:TextBox ID="txtMotivoDemanda" runat="server" TextMode="MultiLine" Rows="2" Style="resize: vertical;" Width="100%" CssClass="form-control" placeholder="Motivo de la demanda"></asp:TextBox>
                                        <small id="small7" visible="false" class="form-text text-muted">
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="txtMotivoDemanda" ValidationGroup="SaveValidation" ErrorMessage="Campo Requerido" ForeColor="red"></asp:RequiredFieldValidator>
                                        </small>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txtConsta" class="my-1 mr-sm-2">Consta de</label>

                                        <asp:TextBox ID="txtConsta" runat="server" TextMode="MultiLine" Rows="2" Style="resize: vertical" Width="100%" CssClass="form-control" placeholder="Consta de"></asp:TextBox>
                                        <small id="small5" visible="false" class="form-text text-muted">
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="txtConsta" ValidationGroup="SaveValidation" ErrorMessage="Campo Requerido" ForeColor="red"></asp:RequiredFieldValidator>
                                        </small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txtFechaConsta">Fecha</label>
                                        <asp:TextBox ID="txtFechaConsta" CssClass="form-control" placeholder="Seleccione la Fecha" runat="server"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="txtFechaConsta_CalendarExtender" Format="dd/MM/yyyy" runat="server" BehaviorID="txtFechaConsta_CalendarExtender" TargetControlID="txtFechaConsta" />
                                        <small id="small6" visible="false" class="form-text text-muted">
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ControlToValidate="txtFechaConsta" ValidationGroup="SaveValidation" ErrorMessage="Campo Requerido" ForeColor="red"></asp:RequiredFieldValidator>
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <asp:Button runat="server" ID="btnUpdate" ValidationGroup="SaveValidation" Text="Guardar" CssClass="btn btn-primary"
                                UseSubmitBehavior="false" OnClick="btnUpdate_Click" />
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="nav-mantenimiento" role="tabpanel" aria-labelledby="nav_mantenimiento">
                    <%-- mantenimiento --%>
                    <div id="mantenimiento" runat="server" visible="true" class="card">
                        <div class="card-body" style="height:30%">
                            <%--<div class="overflow-auto">--%>
                                <div class="form-row">
                                    <div class="col-sm-6">
                                        <div class="form-row" style="text-align: center;">
                                            <div class="col-md-5">
                                                <asp:DropDownList runat="server" ID="cmbTipoFecha" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            <div class="col-md-5">
                                                <asp:TextBox ID="txtFechaTipo" CssClass="form-control" placeholder="Seleccione la Fecha" runat="server"></asp:TextBox>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MM/yyyy" runat="server" BehaviorID="txtFechaTipo_CalendarExtender" TargetControlID="txtFechaTipo" />

                                            </div>
                                            <div class="col-md-1" id="btn1">
                                                <asp:LinkButton ID="btnAddFecha"
                                                    runat="server"
                                                    CssClass="btn btn-primary mb-2" OnClick="btnAddFecha_Click">
                                    <span aria-hidden="true" class="fas fa-check"></span>
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="form-row">
                                            <div class="col-md-11">
                                                <div id="listaFechas">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-row" style="text-align: right;">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-5">
                                                <asp:DropDownList runat="server" ID="cmbObservacionCasacion" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            <div class="col-md-5">
                                                <asp:TextBox ID="txtObservacion" CssClass="form-control" placeholder="Observacion" TextMode="MultiLine" Style="resize: vertical;" runat="server"></asp:TextBox>

                                            </div>
                                            <div class="col-md-1">
                                                <asp:LinkButton ID="btnAddObservacion"
                                                    runat="server" OnClick="btnAddObservacion_Click"
                                                    CssClass="btn btn-primary mb-2">
                                    <span aria-hidden="true" class="fas fa-check"></span>
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="form-row">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-11">
                                                <div id="listaObservaciones"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr />
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="txtResolucion" class="my-1 mr-sm-2">Resolucion</label>
                                            <asp:TextBox ID="txtResolucion" runat="server" TextMode="MultiLine" Rows="2" Style="resize: vertical;" Width="100%" CssClass="form-control" placeholder="Resolucion"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <hr />
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <asp:CheckBox ID="chkCompleto" runat="server" Text="   Completo" />
                                        </div>
                                    </div>
                                </div>
                            <%--</div>--%>
                        </div>
                        <div class="card-footer">
                            <asp:Button runat="server" ID="btnGuardar" Text="Guardar" CssClass="btn btn-primary"
                                UseSubmitBehavior="false" OnClick="btnGuardar_Click" />
                            <asp:Button runat="server" ID="btnRegresar" Text="Regresar" CssClass="btn btn-info"
                                UseSubmitBehavior="false" OnClick="btnRegresar_Click" />
                        </div>
                    </div>
                </div>
            </div>
            <br />

            <asp:TextBox ID="hdnIndex" runat="server" hidden OnTextChanged="hdnIndex_TextChanged"></asp:TextBox>
            <asp:GridView runat="server" ID="grdvSalas" CssClass="table table-responsive-sm table-hover table-sm table-striped" GridLines="None" EmptyDataText="No hay datos para mostrar" ShowHeaderWhenEmpty="true" OnPageIndexChanging="grdvSalas_PageIndexChanging" DataKeyNames="id_casacion" UseAccessibleHeader="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="5">
                <HeaderStyle BackColor="#337ab7" ForeColor="White" />
                <PagerStyle CssClass="pagination-ys" />
                <Columns>
                    <asp:TemplateField ItemStyle-Width="0%">
                        <ItemTemplate>
                            <asp:HiddenField Visible="false" ID="id_casacion" runat="server" Value='<%# Eval("id_casacion") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="0%">
                        <ItemTemplate>
                            <asp:HiddenField Visible="false" ID="id_tipo_casacion" runat="server" Value='<%# Eval("id_tipo_casacion") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="0%">
                        <ItemTemplate>
                            <asp:HiddenField Visible="false" ID="id_juzgado" runat="server" Value='<%# Eval("id_juzgado") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="5%" HeaderText="Codigo">
                        <ItemTemplate>
                            <asp:Label ID="numero_registro" runat="server" Text='<%# Eval("numero_registro") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="20%" HeaderText="Apoderado">
                        <ItemTemplate>
                            <asp:Label ID="nombre_apoderado" runat="server" Text='<%# Eval("nombre_apoderado") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="20%" HeaderText="Demandante">
                        <ItemTemplate>
                            <asp:Label ID="nombre_demandante" runat="server" Text='<%# Eval("nombre_demandante") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="20%" HeaderText="Demandado">
                        <ItemTemplate>
                            <asp:Label ID="nombre_demandado" runat="server" Text='<%# Eval("nombre_demandado") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="5%" HeaderText="Sala">
                        <ItemTemplate>
                            <asp:Label ID="sala" runat="server" Text='<%# Eval("sala") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="10%" HeaderText="Casacion">
                        <ItemTemplate>
                            <asp:Label ID="casacion" runat="server" Text='<%# Eval("casacion") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="20%" HeaderText="Procedencia">
                        <ItemTemplate>
                            <asp:Label ID="procedencia" runat="server" Text='<%# Eval("procedencia") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <%--Botones de eliminar y editar producto...--%>
                            <asp:LinkButton ID="btnEdit"
                                runat="server" ClientIDMode="AutoID"
                                OnClientClick="setRow(this, 'editar');" Font-Underline="true">
                                                            <span aria-hidden="true">Editar</span>
                            </asp:LinkButton>
                            
                            <%--Botones de eliminar y editar producto...--%>
                            <asp:LinkButton ID="btnMantenimiento"
                                runat="server" ClientIDMode="AutoID"
                                OnClientClick="setRow(this, 'mantenimiento');" Font-Underline="true">
                                                            <span aria-hidden="true">Mantenimiento</span>
                            </asp:LinkButton>
                        
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        function ShowMessage(message, messagetype) {
            var cssclass;
            switch (messagetype) {
                case 'Correcto':
                    messagetype = '';
                    cssclass = 'alert-success'
                    break;
                case 'Error':
                    cssclass = 'alert-danger'
                    break;
                case 'Advertencia':
                    cssclass = 'alert-warning'
                    break;
                default:
                    cssclass = 'alert-info'
            }
            $('.modal-backdrop').remove()
            $(document.body).removeClass("modal-open");
            $('#alert_container').append('<div id="alert_div" style="margin: 0 0.5%; -webkit-box-shadow: 3px 4px 6px #999; opacity:100;" class="alert fade in ' + cssclass + '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>' + messagetype + '!</strong> <span>' + message + '</span></div>');
        }

        function setFecha(mensaje) {
            $('#listaFechas').empty();
            $('#listaFechas').append(mensaje);
            
        }
        function setObservacion(mensaje) {
            $('#listaObservaciones').empty();
            $('#listaObservaciones').append( mensaje);
        }
        
        
    </script>
</asp:Content>
