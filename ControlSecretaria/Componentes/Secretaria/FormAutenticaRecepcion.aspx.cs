﻿using Newtonsoft.Json;
using SecretariaConnection;
using SecretariaConnection.EFModel;
using SecretariaConnection.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ControlSecretaria.Componentes.Secretaria
{
    public partial class FormAutenticaRecepcion : System.Web.UI.Page
    {
        Repositorio conexion = new Repositorio();
        private DataTable dt;
        string mensaje;
        public void showCodigo(string Message, object type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
        }
        public enum MessageType { Correcto, Error, Informacion, Advertencia };

        protected void ShowMessage(string Message, object type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "toastrs('" + Message + "','" + type + "');", true);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                llenarSolicitudes();
                
                dt = new DataTable();
                llenarTabla();
                //mostrarFechas();
            } else TabName.Value = Request.Form[TabName.UniqueID];
        }

        /*private void llenarProcesar()
        {
            try
            {
                //txtId.Text = "0";
                var ds = conexion.getCertificador();
                cmbAutenticador.DataSource = ds;
                cmbAutenticador.DataValueField = "id_certificador";
                cmbAutenticador.DataTextField = "nombre_certificador";
                cmbAutenticador.DataBind();
                cmbAutenticador.Items.Insert(0, new ListItem("Seleccione un Certificador", "0"));
                //comboTribunal.SelectedIndex = 0;

                var ds2 = conexion.getSecretario();
                cmbSecretario.DataSource = ds2;
                cmbSecretario.DataValueField = "id_secretario";
                cmbSecretario.DataTextField = "nombre";
                cmbSecretario.DataBind();
                cmbSecretario.Items.Insert(0, new ListItem("Seleccione un Secretario(a)", "0"));

                var ds3 = conexion.getSecretario();
                cmbPuestoSecretario.DataSource = ds3;
                cmbPuestoSecretario.DataValueField = "id_secretario";
                cmbPuestoSecretario.DataTextField = "puesto";
                cmbPuestoSecretario.DataBind();
                cmbPuestoSecretario.Items.Insert(0, new ListItem("Seleccione un Puesto Secretario(a)", "0"));

                var ds4 = conexion.getCertificador();
                cmbPuestoAutenticador.DataSource = ds4;
                cmbPuestoAutenticador.DataValueField = "id_certificador";
                cmbPuestoAutenticador.DataTextField = "puesto";
                cmbPuestoAutenticador.DataBind();
                cmbPuestoAutenticador.Items.Insert(0, new ListItem("Seleccione un Puesto Certificador(a)", "0"));
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, MessageType.Error);
            }
        }*/

        private void llenarSolicitudes()
        {
            try
            {
                //txtId.Text = "0";
                var ds = conexion.getTipoSolicitud();
                cmbSolicitud.DataSource = ds;
                cmbSolicitud.DataValueField = "id_tipo_solicitud";
                cmbSolicitud.DataTextField = "descripcion";
                cmbSolicitud.DataBind();
                cmbSolicitud.Items.Insert(0, new ListItem("Seleccione untipo de solicitud", "0"));
                //comboTribunal.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, MessageType.Error);
            }
        }

        public void llenarMantenimiento(int id_autentica, int id_transaccion) //IMPORTANTE 
        {
            //var ds = conexion.getTipoFecha(1);
            var ds3 = conexion.getFechas2(id_autentica, id_transaccion);
            if (ds3.Count() > 0)
            {
                cmbTipoFecha.DataSource = ds3;
                cmbTipoFecha.DataValueField = "id_tipo_fecha";
                cmbTipoFecha.DataTextField = "descripcion";
                cmbTipoFecha.DataBind();
            }
            cmbTipoFecha.Items.Insert(0, new ListItem("Seleccione una Fecha", "0"));

            var ds2 = conexion.getObservaciones2(id_autentica, id_transaccion);
            if (ds2.Count() > 0)
            {
                cmbObservacionCasacion.DataSource = ds2;
                cmbObservacionCasacion.DataValueField = "id_tipo_observacion";
                cmbObservacionCasacion.DataTextField = "descripcion";
                cmbObservacionCasacion.DataBind();
            }
            cmbObservacionCasacion.Items.Insert(0, new ListItem("Seleccione una Observacion", "0"));
        } 

        private void llenarTabla()
        {
            try
            {
                dt.Columns.AddRange(new DataColumn[] {
                            new DataColumn("id_autentica"),
                             new DataColumn("id_tipo_solicitud")
                            ,new DataColumn("numero_autentica")
                            ,new DataColumn("nombre_apoderado")
                            ,new DataColumn("nombre_autenticador")
                            ,new DataColumn("tipo_solicitud")
                        });

                var ds = conexion.getAutenticas();
                foreach (AutenticaRecepcionViewModel lista in ds)
                {
                    dt.Rows.Add(lista.id_autentica_recepcion, lista.id_tipo_solicitud, lista.numero_autentica, lista.nombre_apoderado, lista.nombre_autenticador,
                        lista.tipoSolicitud.descripcion);
                }
                grdvSalas.DataSource = dt;
                grdvSalas.DataBind();
                grdvSalas.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, MessageType.Error);
            }
        }

        protected void grdvSalas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void hdnIndex_TextChanged(object sender, EventArgs e)
        {
            string[] s = hdnIndex.Text.ToString().Split(';');
            if (s[0].Equals("editar"))
                btnEdit_Click(sender, e);
            else
            {
                btnEdit_Click(sender, e);
                procesar();
            }
        }

        public void mostrarFechas(int id)
        {
            string mensaje = getFechas(id);
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "setFecha('" + mensaje + "');", true);
        }
        public string getMensaje(string tipo, string fecha)
        {
            string mensaje = "<strong>" + tipo + "</strong><br/><p>" + fecha + "</p><hr/>";
            return mensaje;
        }
        public string getFechas(int id)
        {
            string mensaje = "";
            var ds = conexion.getFechasAutenticas(id);
            foreach (FechaAutenticaViewModel fecha in ds)
            {
                mensaje += getMensaje(fecha.tipoFecha.descripcion, fecha.descripcion);
            }

            return mensaje;
        }
        public string getObservaciones(int id)
        {
            string mensaje = "";
            var ds = conexion.getObservacionesAutenticas(id);
            foreach (ObservacionAutenticaViewModel obs in ds)
            {
                mensaje += getMensaje(obs.tipoObservacion.descripcion, obs.descripcion);
            }

            return mensaje;
        }
        public void mostrarObservaciones(int id)
        {
            string mensaje = getObservaciones(id);
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "setObservacion('" + mensaje + "');", true);
        }
        private void procesar()
        {
            string[] s = hdnIndex.Text.Split(';');
            int index = Convert.ToInt32(s[1]);

            GridViewRow row = grdvSalas.Rows[index - 1];
            int id = Convert.ToInt32((row.FindControl("id_autentica") as HiddenField).Value);

            cmbSecretario.Items.Clear();
            
            cmbPuestoSecretario.Items.Clear();
            //llenarProcesar();

            txtCodigo.Text = Convert.ToString(id);
            mantenimiento.Visible = true;
            nav_registro.Visible = true;
            btnUpdate.Visible = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "getCertificador();", true);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static List<CertificadorViewModel> listaCertificador()
        {
            Repositorio repo = new Repositorio();
            var ds = repo.getCertificador();
            return ds;
        }

        private void limpiarCampos()
        {
            hdnIndex.Text = String.Empty;
            txtCodigo.Text = String.Empty;
            mantenimiento.Visible = false;
            nav_registro.Visible = false;
            btnUpdate.Visible = true;
            btnUpdate.Text = "Guardar";
            panelFechas.Visible = false;
            txtSolicitante.Text = String.Empty;
            cmbSolicitud.SelectedIndex = 0;
            txtRecibo.Text = String.Empty;
            txtFirma.Text = String.Empty;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            string[] s = hdnIndex.Text.Split(';');
            int index = Convert.ToInt32(s[1]);
            //limpiarCampos();
            GridViewRow row = grdvSalas.Rows[index - 1];
            //txtCodigo.Text = (row.FindControl("id_tipo_observacion") as HiddenField).Value;
            //txtNombre.Text = (row.FindControl("descripcion") as Label).Text;
            int id = Convert.ToInt32((row.FindControl("id_autentica") as HiddenField).Value);
            txtSolicitante.Text = (row.FindControl("nombre_apoderado") as Label).Text;

            txtCodigo.Text = Convert.ToString(id);
            var tmp = conexion.buscarAutenticaRecepcion(id);
            cmbSolicitud.SelectedValue = Convert.ToString( tmp.id_tipo_solicitud);
            txtRecibo.Text = tmp.numero_recibo;
            txtFirma.Text = (row.FindControl("nombre_autenticador") as Label).Text;
            llenarMantenimiento(id, 2);
            mostrarFechas(id);
            mostrarObservaciones(id);

            autenticaLbl.InnerText = "Autentica No. " + tmp.numero_autentica;

            panelFechas.Visible = true;


            btnUpdate.Text = "Modificar";
        }

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            limpiarCampos();
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            DateTime fecha = DateTime.Now;
            int correlativo = conexion.correlativoAutenticas(fecha.Year.ToString()) + 1;
            string numero = correlativo + "-" + fecha.Year.ToString().Substring(2);
            try
            {
                autentica_recepcion autentica = new autentica_recepcion()
                {
                    auto = "",
                    denegar = "",
                    fecha_registro = DateTime.Now,
                    id_tipo_solicitud = Convert.ToInt32(cmbSolicitud.SelectedValue),
                    autorizada = "",
                    nombre_apoderado = txtSolicitante.Text,
                    nombre_autenticador = txtFirma.Text,
                    numero_recibo = txtRecibo.Text,
                    observacion = "",
                    usuario_registro = "admin"
                    
                };
                if (btnUpdate.Text.Equals("Guardar"))
                {

                    autentica.numero_autentica = numero;
                    int new_id = conexion.setAutenticas(autentica, System.Data.Entity.EntityState.Added);
                    ShowMessage("Registro guardado exitosamente", MessageType.Correcto);
                    showCodigo("Autentica número <strong>" + numero + "</strong> en revisión", MessageType.Informacion);
                }
                else
                {
                    var tmp = conexion.buscarAutenticaRecepcion(Convert.ToInt32(txtCodigo.Text));
                    autentica.id_autentica_recepcion = Convert.ToInt32(txtCodigo.Text);
                    autentica.numero_autentica = tmp.numero_autentica;
                    conexion.setAutenticas(autentica, System.Data.Entity.EntityState.Modified);
                    ShowMessage("Registro modificado exitosamente", MessageType.Correcto);
                    showCodigo("Autentica número <strong>" + autentica.numero_autentica+  "</strong> actualizada", MessageType.Informacion);
                }
                limpiarCampos();
            }
            catch (Exception ex)
            {
                ShowMessage("error " + ex.Message, MessageType.Error);
            }
        }

        protected void btnAddFecha_Click(object sender, EventArgs e)
        {

            fecha_autentica fecha = new fecha_autentica()
            {
                id_autentica_recepcion = Convert.ToInt32(txtCodigo.Text),
                descripcion = txtFechaTipo.Text,
                id_tipo_fecha = Convert.ToInt32(cmbTipoFecha.SelectedValue),
                estado = true,
                fecha_registro = DateTime.Now,
                usuario_registro = "admin"
            };

            conexion.setFechaAutentica(fecha, System.Data.Entity.EntityState.Added);
            mostrarFechas(Convert.ToInt32(txtCodigo.Text));
            mostrarObservaciones(Convert.ToInt32(txtCodigo.Text));
            txtFechaTipo.Text = String.Empty;
            cmbTipoFecha.Items.Clear();
            cmbObservacionCasacion.Items.Clear();
            llenarMantenimiento(Convert.ToInt32(txtCodigo.Text), 2);
            cmbTipoFecha.SelectedIndex = 0;
        }

        protected void btnAddObservacion_Click(object sender, EventArgs e)
        {
            observacion_autentica observacion = new observacion_autentica()
            {
                id_autentica_recepcion = Convert.ToInt32(txtCodigo.Text),
                descripcion = txtObservacion.Text,
                id_tipo_observacion = Convert.ToInt32(cmbObservacionCasacion.SelectedValue),
                estado = true,
                fecha_registro = DateTime.Now,
                usuario_registro = "admin"
            };
            conexion.setObservacionAutentica(observacion, System.Data.Entity.EntityState.Added);
            txtObservacion.Text = String.Empty;
            cmbObservacionCasacion.SelectedIndex = 0;
            cmbTipoFecha.Items.Clear();
            cmbObservacionCasacion.Items.Clear();
            llenarMantenimiento(Convert.ToInt32(txtCodigo.Text), 2);
            mostrarObservaciones(Convert.ToInt32(txtCodigo.Text));
            mostrarFechas(Convert.ToInt32(txtCodigo.Text));
        }

    }
}